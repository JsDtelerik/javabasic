package DayEleven;

import java.util.Scanner;

public class Building {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int floors = Integer.parseInt(scan.nextLine());
        int rooms = Integer.parseInt(scan.nextLine());
        char roomTypes;

        for (int i = floors; i >= 1; i--){
                if (i == floors){
                    roomTypes = 'L';
                }else if (i % 2 == 0){
                    roomTypes = 'O';
                }else{
                    roomTypes = 'A';
                }
            for (int j = 0; j < rooms; j++ ){
                System.out.printf("%c%d%d ", roomTypes, i, j);
            }
            System.out.println();
        }
    }
}
