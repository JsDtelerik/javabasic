package DayEleven;

import java.util.Scanner;

public class CinemaOrg {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String input=scanner.nextLine();
        int totalTicketsSold=0;
        int studentTickets=0;
        int kidsTickets=0;
        int standardTickets=0;
        while (!input.equals("Finish")){
            String movieName=input;
            int seats=Integer.parseInt(scanner.nextLine());
            String text=scanner.nextLine();
            int counterOfSoldMovieTickets=0;
            while (!text.equals("End")){
                switch (text){
                    case "standard":standardTickets++;break;
                    case "kid":kidsTickets++;break;
                    case "student":studentTickets++;break;
                }
                counterOfSoldMovieTickets++;

                if(counterOfSoldMovieTickets>=seats){
                    break;
                }
                text=scanner.nextLine();
            }
            totalTicketsSold+=counterOfSoldMovieTickets;
            double roomFilled=counterOfSoldMovieTickets*1.0/seats*100;
            System.out.printf("%s - %.2f%% full.%n",movieName,roomFilled);
            input=scanner.nextLine();
        }
        double studentTicketsPercentage = studentTickets*1.0 / totalTicketsSold *100;
        double kidsTicketsPercentage = kidsTickets*1.0 / totalTicketsSold *100;
        double standardTicketsPercentage = standardTickets*1.0 / totalTicketsSold *100;
        System.out.printf("Total tickets: %d\n" +
                        "%.2f%% student tickets.\n" +
                        "%.2f%% standard tickets.\n" +
                        "%.2f%% kids tickets.\n",totalTicketsSold,studentTicketsPercentage,
                standardTicketsPercentage, kidsTicketsPercentage);
    }
}


