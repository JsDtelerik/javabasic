package DayEleven;

import java.util.Scanner;

public class CinemaTickets {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String movie = scan.nextLine();
        String movieName = "";
        int totalTicketsSold = 0;
        int allStudentTicket = 0;
        int allStandardTicket = 0;
        int allKidTicket = 0;
        while (!movie.equals("Finish")){
            int seats = Integer.parseInt(scan.nextLine());
            String input = scan.nextLine();
            movieName = movie;
            int soldTicketsCurrentMovie =0;
            int studentTicket = 0;
            int standardTicket = 0;
            int kidTicket = 0;
            while (!input.equals("End")){
               switch (input){
                   case "student": studentTicket++; break;
                   case "standard": standardTicket++; break;
                   case "kid": kidTicket++; break;
               }
                soldTicketsCurrentMovie = standardTicket+studentTicket+kidTicket;
                if (soldTicketsCurrentMovie>=seats){
                    break;
                }
                input = scan.nextLine();
            }
            totalTicketsSold += soldTicketsCurrentMovie;
            allStudentTicket+=studentTicket;
            allStandardTicket += standardTicket;
            allKidTicket += kidTicket;
            double soldTicketsPercent = 1.0*soldTicketsCurrentMovie/seats*100;
            System.out.printf("%s - %.02f%% full.%n", movieName, soldTicketsPercent);
            movie = scan.nextLine();
            }
                double studentTicketsPercentage = 1.0*allStudentTicket/totalTicketsSold*100;
                double standardTicketsPercentage = 1.0*allStandardTicket/totalTicketsSold*100;
                double kidsTicketsPercentage = 1.0*allKidTicket/totalTicketsSold*100;
                System.out.printf("Total tickets: %d %n" +
                        "%.02f%% student tickets.%n" +
                        "%.02f%% standard tickets.%n" +
                        "%.02f%% kids tickets.%n", totalTicketsSold, studentTicketsPercentage, standardTicketsPercentage, kidsTicketsPercentage );
            }
        }
