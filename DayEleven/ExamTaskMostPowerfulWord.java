package DayEleven;

import java.util.Scanner;

public class ExamTaskMostPowerfulWord {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String word = scanner.nextLine();
        int wordPower = 0;
        int mostPowerfulWord = Integer.MIN_VALUE;
        String mostPowerfulWordText = "";
        boolean vowelFound = false;
        while (!word.equals("End of words")) {
            wordPower = 0;
            vowelFound = false;
            for (int l = 0; l < word.length(); l++) {
                char letter = word.charAt(l);
                if (l == 0) {
                    char letterIgnoreCase = Character.toLowerCase(letter);
                    if (letterIgnoreCase == 'a' ||
                            letterIgnoreCase == 'e' ||
                            letterIgnoreCase == 'i' ||
                            letterIgnoreCase == 'o' ||
                            letterIgnoreCase == 'u' ||
                            letterIgnoreCase == 'y'
                    ) {
                        vowelFound = true;
                    }
                }
                wordPower += letter;
            }
            if (vowelFound) {
                wordPower *= word.length();
            } else {
                wordPower /= Math.floor(word.length());
            }
            if (wordPower > mostPowerfulWord) {
                mostPowerfulWord = wordPower;
                mostPowerfulWordText = word;
            }

            word = scanner.nextLine();
        }
        System.out.printf("The most powerful word is %s - %d", mostPowerfulWordText, mostPowerfulWord);
    }
}


