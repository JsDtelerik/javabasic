package DayEleven;
import java.util.Scanner;
public class Lesson {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int eggsInTheBeginning = Integer.parseInt(scan.nextLine());

        String input = scan.nextLine();


        int soldEggs = 0;
        boolean isClosed = false;
        while (!input.equals("Close")) {
            String neededNumOfEggs = scan.nextLine();
            int eggs = Integer.parseInt(neededNumOfEggs);

            if (input.equals("Buy")) {
                boolean isThereEnough = eggsInTheBeginning >= eggs;

                if (isThereEnough) {
                    eggsInTheBeginning -= eggs;
                    soldEggs += eggs;

                } else {
                    System.out.printf("Not enough eggs in store!%nYou can buy only %d.", eggsInTheBeginning);
                    break;
                }
            }
            if (input.equals("Fill")) {
                eggsInTheBeginning += eggs;
            }

            input = scan.nextLine();
            if (input.equals("Close")){
                isClosed = true;
            }
        }
        if (isClosed)
        System.out.printf("Store is closed!%n%d eggs sold.", soldEggs);
    }
}