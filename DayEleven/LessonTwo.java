package DayEleven;

import java.util.Scanner;

public class LessonTwo {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        while (!input.equals("End")) {

            double money = Double.parseDouble(scanner.nextLine());
            double savings = 0;

            while (savings < money) {
                savings += Double.parseDouble(scanner.nextLine());
            }
            System.out.printf("Going to %s!%n", input);

            input = scanner.nextLine();
        }


    }
}