package DayEleven;

import java.util.Scanner;

public class SumOfTwoNumbers {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int nOne = Integer.parseInt(scan.nextLine());
        int nTwo = Integer.parseInt(scan.nextLine());
        int magicNum = Integer.parseInt(scan.nextLine());
        int count = 0;
        boolean isFound = false;
        label:
        for (int i = nOne; i <= nTwo; i++) {
            for (int j = nOne; j <= nTwo; j++) {
                count++;
                if (i + j == magicNum) {
                    isFound = true;
                    System.out.printf("Combination N:%d (%d + %d = %d)", count, i, j, magicNum);
                    break label;
                }
            }
        }
        if(!isFound){
            System.out.printf("%d combinations - neither equals %d", count, magicNum);
    }
    }
}