package DayFive;

import java.util.Scanner;

public class DayFiveLesson {

    public static void main(String[] args) {


        Scanner scan = new Scanner(System.in);


        double income = Double.parseDouble(scan.nextLine());
        double score = Double.parseDouble(scan.nextLine());
        double mrz = Double.parseDouble(scan.nextLine());

        // izchislenia

        double socialSch = mrz * 0.35;
        double excellntSch = 25 * score;
        String result = "";

        // proverki


        if (income < mrz) {
            if (score >= 4.5 && score < 5.5) {
                result = String.format("You get a Social scholarship %.0f BGN", Math.floor(socialSch));
            } else if (score >= 5.5 && excellntSch >= socialSch) {
                result = String.format("You get a scholarship for excellent results %.0f BGN", Math.floor(excellntSch));
            } else if (score >= 5.5 && excellntSch < socialSch) {
                result = String.format("You get a Social scholarship %.0f BGN", Math.floor(socialSch));
            } else
                result = "You cannot get a scholarship!";
        } else if (score >= 5.5 && income >= mrz)
            result = String.format("You get a scholarship for excellent results %.0f BGN", Math.floor(excellntSch));
        else
            result = "You cannot get a scholarship!";

        System.out.println(result);
    }


}
