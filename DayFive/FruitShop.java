package DayFive;

import java.util.Scanner;

public class FruitShop {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String fruit = scan.nextLine();
        String day = scan.nextLine();
        double quantity = Double.parseDouble(scan.nextLine());
        double price = 0.0;
        if (day.equalsIgnoreCase("sunday") || day.equalsIgnoreCase("saturday")) {
            switch (fruit) {
                case "banana":
                    price = quantity * 2.7;
                    System.out.printf("%.2f",price);
                    break;
                case "apple":
                    price = quantity * 1.25;
                    System.out.printf("%.2f",price);
                    break;
                case "orange":
                    price = quantity * 0.90;
                    System.out.printf("%.2f",price);
                    break;
                case "grapefruit":
                    price = quantity * 1.6;
                    System.out.printf("%.2f",price);
                    break;
                case "kiwi":
                    price = quantity * 3.00;
                    System.out.printf("%.2f",price);
                    break;
                case "pineapple":
                    price = quantity * 5.6;
                    System.out.printf("%.2f",price);
                    break;
                case "grapes":
                    price = quantity * 4.2;
                    System.out.printf("%.2f",price);
                    break;
                default:
                    System.out.printf("error"); break;


            }

        } else if (day.equalsIgnoreCase("Monday") || day.equalsIgnoreCase("Tuesday") || day.equalsIgnoreCase("Wednesday")
                || day.equalsIgnoreCase("Thursday") || day.equalsIgnoreCase("friday")) {
            switch (fruit) {
                case "banana":
                    price = quantity * 2.5;
                    System.out.printf("%.2f",price);
                    break;
                case "apple":
                    price = quantity * 1.20;
                    System.out.printf("%.2f",price);
                    break;
                case "orange":
                    price = quantity * 0.85;
                    System.out.printf("%.2f",price);
                    break;
                case "grapefruit":
                    price = quantity * 1.45;
                    System.out.printf("%.2f",price);
                    break;
                case "kiwi":
                    price = quantity * 2.70;
                    System.out.printf("%.2f",price);
                    break;
                case "pineapple":
                    price = quantity * 5.5;
                    System.out.printf("%.2f",price);
                    break;
                case "grapes":
                    price = quantity * 3.85;
                    System.out.printf("%.2f",price);
                    break;
                default:
                    System.out.printf("error"); break;

            }

            }else
            System.out.println("error");

        }

    }




