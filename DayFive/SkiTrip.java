package DayFive;

import java.util.Scanner;

public class SkiTrip {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int realdays = Integer.parseInt(scan.nextLine());
        String room = scan.nextLine();
        String evaluation = scan.nextLine();
        int days = realdays-1;
        double result = 0.0;
        if (days<10){
            switch (room){
                case "room for one person": result=days*18.00; break;
                case "apartment": result=(days*25.00)-((days*25.00)*0.3); break;
                case "president apartment": result=(days*35.00)-((days*35.00)*0.1);break;

            }
        }else if (days>=10 && days<=15){
            switch (room){
                case "room for one person": result=days*18.00; break;
                case "apartment": result=(days*25.00)-((days*25.00)*0.35); break;
                case "president apartment": result=(days*35.00)-((days*35.00)*0.15);break;
            }
        }else if (days>15){
            switch (room){
                case "room for one person": result=days*18.00; break;
                case "apartment": result=(days*25.00)-((days*25.00)*0.50); break;
                case "president apartment": result=(days*35.00)-((days*35.00)*0.20);break;
            }
        }
        if (evaluation.equalsIgnoreCase("positive")){
             result *=1.25;
        }

        if (evaluation.equalsIgnoreCase("negative")){
            result *=0.9;
        }
        System.out.printf("%.2f", result);

    }
}
