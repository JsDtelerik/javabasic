package DayFive;

import java.util.Scanner;

public class TradeCommissions {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String town = scan.nextLine();
        double s = Double.parseDouble(scan.nextLine());
        double commission = 0.0;
        String result = "";
        if (s>=0 && s<=500){
            switch (town){
                case "Sofia": commission=s*0.05; result =String.format("%.2f", commission); break;
                case "Varna": commission=s*0.045; result =String.format("%.2f", commission); break;
                case "Plovdiv": commission=s*0.055; result =String.format("%.2f", commission); break;
                default:
                    result = "error";break;

            }

        }else if (s>500 && s<=1000){
            switch (town){
                case "Sofia": commission=s*0.07; result =String.format("%.2f", commission); break;
                case "Varna": commission=s*0.075; result =String.format("%.2f", commission); break;
                case "Plovdiv": commission=s*0.08; result =String.format("%.2f", commission); break;
                default:
                    result = "error";break;

            }

        }else if (s>1000 && s<= 10000){
            switch (town){
                case "Sofia": commission=s*0.08; result =String.format("%.2f", commission); break;
                case "Varna": commission=s*0.1; result =String.format("%.2f", commission); break;
                case "Plovdiv": commission=s*0.12; result =String.format("%.2f", commission); break;
                default:
                    result = "error";break;

            }
        }else if(s>10000){
            switch (town){
                case "Sofia": commission=s*0.12; result =String.format("%.2f", commission); break;
                case "Varna": commission=s*0.13; result =String.format("%.2f", commission); break;
                case "Plovdiv": commission=s*0.145; result =String.format("%.2f", commission); break;
                default:
                    result = "error";break;
            }
        }else{
            result = "error";
        }

        System.out.println(result);

    }
}
