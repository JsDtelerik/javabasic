package DayFour.AdditionalTask;

import java.util.Scanner;

public class Firm {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int hoursNeeded = Integer.parseInt(scan.nextLine());
        int projectDays = Integer.parseInt(scan.nextLine());
        int overTimeWorkers = Integer.parseInt(scan.nextLine());
        double trainingHours = projectDays*0.1;
        double workingHours = (projectDays-trainingHours)*8;
        double overTime = (overTimeWorkers*2)*projectDays;
        double totalHours = Math.floor(workingHours+overTime);
        String output = "";
        if (totalHours>=hoursNeeded){
            output = String.format("Yes!%.0f hours left.", totalHours-hoursNeeded);
        }else
            output = String.format("Not enough time!%.0f hours needed.", hoursNeeded-totalHours);


        System.out.println(output);
    }
}
