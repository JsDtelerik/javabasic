package DayFour.AdditionalTask;

import java.util.Scanner;

public class FlowerShop {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int magnolias = Integer.parseInt(scan.nextLine());
        int hyacinth = Integer.parseInt(scan.nextLine());
        int roses = Integer.parseInt(scan.nextLine());
        int cactus = Integer.parseInt(scan.nextLine());
        double giftPrice = Double.parseDouble(scan.nextLine());
        double incomes = ((magnolias*3.25)+(hyacinth*4.00)+(roses*3.50)+(cactus*8.00));
        double taxes = incomes*0.05;
        double realIncomes = incomes-taxes;
           String possibilityToBuyGift = "";

        if (realIncomes>=giftPrice){
            possibilityToBuyGift = String.format("She is left with %.0f leva.", Math.floor(realIncomes-giftPrice));
        }else
            possibilityToBuyGift = String.format("She will have to borrow %.0f leva.", Math.ceil(giftPrice-realIncomes));

        System.out.println(possibilityToBuyGift);


    }
}
