package DayFour.AdditionalTask;

import javax.swing.*;
import java.util.Objects;
import java.util.Scanner;

public class FuelTank {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String fuelType = scan.nextLine();
        double litersFuel = Double.parseDouble(scan.nextLine());

        String fuelTankIndicator = "";

        if (fuelType.equalsIgnoreCase("Gasoline") && litersFuel >= 25) {
            fuelTankIndicator = "You have enough gasoline.";
        }else if (fuelType.equalsIgnoreCase("Diesel") && litersFuel>=25) {
            fuelTankIndicator = "You have enough diesel.";
        }else if (fuelType.equalsIgnoreCase("Gas") && litersFuel>=25) {
            fuelTankIndicator = "You have enough gas.";
        }else if (fuelType.equalsIgnoreCase("Gasoline") && litersFuel<25){
            fuelTankIndicator = "Fill your tank with gasoline!";
        }else if (fuelType.equalsIgnoreCase("diesel") && litersFuel<25)   {
            fuelTankIndicator = "Fill your tank with diesel!";
        }else if (fuelType.equalsIgnoreCase("Gas") && litersFuel<25)   {
            fuelTankIndicator = "Fill your tank with gas!";
        } else
            fuelTankIndicator = "Invalid fuel!";

        System.out.println(fuelTankIndicator);


    }

}