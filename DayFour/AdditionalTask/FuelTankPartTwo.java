package DayFour.AdditionalTask;

import java.util.Scanner;

public class FuelTankPartTwo {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String fuelType = scan.nextLine();
        double litersFuel = Double.parseDouble(scan.nextLine());
        String clubCard = scan.nextLine();
        double paidPriceForFuel = 0.0;
        String resultInBgn = "";
        if(fuelType.equalsIgnoreCase("Diesel") &&
                clubCard.equalsIgnoreCase("yes") && litersFuel>=20 && litersFuel<=25){
                double fuel = (2.33-0.12)*litersFuel;
                paidPriceForFuel = fuel-(fuel*0.08);
                resultInBgn = String.format("%.2f lv.",paidPriceForFuel);
        }else if(fuelType.equalsIgnoreCase("Diesel") &&
                clubCard.equalsIgnoreCase("no") && litersFuel>=20 && litersFuel<=25){
            double fuel = 2.33*litersFuel;
            paidPriceForFuel = fuel-(fuel*0.08);
            resultInBgn = String.format("%.2f lv.",paidPriceForFuel);
        }else if(fuelType.equalsIgnoreCase("Diesel") &&
                clubCard.equalsIgnoreCase("yes") && litersFuel>25){
            double fuel = (2.33-0.12)*litersFuel;
            paidPriceForFuel = fuel-(fuel*0.10);
            resultInBgn = String.format("%.2f lv.",paidPriceForFuel);
        }else if(fuelType.equalsIgnoreCase("Diesel") &&
                clubCard.equalsIgnoreCase("no") && litersFuel>25){
            double fuel = 2.33*litersFuel;
            paidPriceForFuel = fuel-(fuel*0.10);
            resultInBgn = String.format("%.2f lv.",paidPriceForFuel);
        }else if(fuelType.equalsIgnoreCase("Diesel") &&
                clubCard.equalsIgnoreCase("no") && litersFuel<20){
                paidPriceForFuel = 2.33*litersFuel;
            resultInBgn = String.format("%.2f lv.",paidPriceForFuel);

        }else if(fuelType.equalsIgnoreCase("Diesel") &&
                clubCard.equalsIgnoreCase("yes") && litersFuel<20){
            paidPriceForFuel = (2.33-0.12)*litersFuel;
            resultInBgn = String.format("%.2f lv.",paidPriceForFuel);
        }else if(fuelType.equalsIgnoreCase("Gasoline") &&
                clubCard.equalsIgnoreCase("yes") && litersFuel>=20 && litersFuel<=25){
            double fuel = (2.22-0.18)*litersFuel;
            paidPriceForFuel = fuel-(fuel*0.08);
            resultInBgn = String.format("%.2f lv.",paidPriceForFuel);
        }else if(fuelType.equalsIgnoreCase("Gasoline") &&
                clubCard.equalsIgnoreCase("no") && litersFuel>=20 && litersFuel<=25){
            double fuel = 2.22*litersFuel;
            paidPriceForFuel = fuel-(fuel*0.08);
            resultInBgn = String.format("%.2f lv.",paidPriceForFuel);
        }else if(fuelType.equalsIgnoreCase("Gasoline") &&
                clubCard.equalsIgnoreCase("yes") && litersFuel>25){
            double fuel = (2.22-0.18)*litersFuel;
            paidPriceForFuel = fuel-(fuel*0.10);
            resultInBgn = String.format("%.2f lv.",paidPriceForFuel);
        }else if(fuelType.equalsIgnoreCase("Gasoline") &&
                clubCard.equalsIgnoreCase("no") && litersFuel>25){
            double fuel = 2.22*litersFuel;
            paidPriceForFuel = fuel-(fuel*0.10);
            resultInBgn = String.format("%.2f lv.",paidPriceForFuel);
        }else if(fuelType.equalsIgnoreCase("Gasoline") &&
                clubCard.equalsIgnoreCase("no") && litersFuel<20){
            paidPriceForFuel = 2.22*litersFuel;
            resultInBgn = String.format("%.2f lv.",paidPriceForFuel);
        }else if(fuelType.equalsIgnoreCase("Gasoline") &&
                clubCard.equalsIgnoreCase("yes") && litersFuel<20){
            paidPriceForFuel = (2.22-0.18)*litersFuel;
            resultInBgn = String.format("%.2f lv.",paidPriceForFuel);
        }else if(fuelType.equalsIgnoreCase("Gas") &&
                clubCard.equalsIgnoreCase("no") && litersFuel<20){
            paidPriceForFuel = 0.93*litersFuel;
            resultInBgn = String.format("%.2f lv.",paidPriceForFuel);
        }else if(fuelType.equalsIgnoreCase("Gas") &&
                clubCard.equalsIgnoreCase("yes") && litersFuel<20){
            paidPriceForFuel = (0.93-0.08)*litersFuel;
            resultInBgn = String.format("%.2f lv.",paidPriceForFuel);
        }else if(fuelType.equalsIgnoreCase("Gas") &&
                clubCard.equalsIgnoreCase("Yes") && litersFuel>=20 && litersFuel<=25){
            double fuel = (0.93-0.08)*litersFuel;
            paidPriceForFuel = fuel-(fuel*0.08);
            resultInBgn = String.format("%.2f lv.",paidPriceForFuel);
        }else if(fuelType.equalsIgnoreCase("Gas") &&
                clubCard.equalsIgnoreCase("no") && litersFuel>=20 && litersFuel<=25){
            double fuel = 0.93*litersFuel;
            paidPriceForFuel = fuel-(fuel*0.08);
            resultInBgn = String.format("%.2f lv.",paidPriceForFuel);
        }else if(fuelType.equalsIgnoreCase("Gas") &&
                clubCard.equalsIgnoreCase("yes") && litersFuel>25){
            double fuel = (0.93-0.08)*litersFuel;
            paidPriceForFuel = fuel-(fuel*0.10);
            resultInBgn = String.format("%.2f lv.",paidPriceForFuel);
        }else if(fuelType.equalsIgnoreCase("Gas") &&
                clubCard.equalsIgnoreCase("no") && litersFuel>25) {
            double fuel = 0.93 * litersFuel;
            paidPriceForFuel = fuel - (fuel * 0.10);
            resultInBgn = String.format("%.2f lv.",paidPriceForFuel);


        }

        System.out.println(resultInBgn);

    }


}
