package DayFour.AdditionalTask;

import java.util.Scanner;

public class Harvest {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int vineyard = Integer.parseInt(scan.nextLine());
        double grapePerSqM = Double.parseDouble(scan.nextLine());
        int litersWineNeeded = Integer.parseInt(scan.nextLine());
        int workersNeeded = Integer.parseInt(scan.nextLine());
        double producedWine =((0.4*(vineyard*grapePerSqM))/2.5);
        double totalWine = 0.0;
        double wineLeft = 0.0;
        double wineNeeded = 0.0;
        double winePerWorker = 0.0;
        String output = "";
        if (producedWine>=litersWineNeeded){
            totalWine = Math.floor(producedWine);
            wineLeft = Math.ceil(producedWine-litersWineNeeded);
             winePerWorker = Math.ceil(wineLeft/workersNeeded);
             output = String.format("Good harvest this year! Total wine: %.0f liters.%n" +
                    "%.0f liters left -> %.0f liters per person.", totalWine, wineLeft, winePerWorker);
        }else{
            wineNeeded = Math.floor(litersWineNeeded-producedWine);
            output = String.format("It will be a tough winter! More %.0f liters wine needed.", wineNeeded);

        }
        System.out.println(output);
    }
}
