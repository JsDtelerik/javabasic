package DayFour.AdditionalTask;

import java.util.Scanner;

public class Pets {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int days = Integer.parseInt(scan.nextLine());
        int foodKg = Integer.parseInt(scan.nextLine());
        double dogFoodPerDayKg = Double.parseDouble(scan.nextLine());
        double  catFoodPerDayKg = Double.parseDouble(scan.nextLine());
        double turtleFoodPerDayGr = Double.parseDouble(scan.nextLine());
        double dogFoodForTheEntirePeriodKg = dogFoodPerDayKg*days;
        double catFoodForTheEntirePeriodKg = catFoodPerDayKg*days;
        double turtleFoodForTheEntirePeriodKg = (turtleFoodPerDayGr*days)/1000;
        double foodNeededKg = dogFoodForTheEntirePeriodKg+catFoodForTheEntirePeriodKg+turtleFoodForTheEntirePeriodKg;
        String output = "";

        if (foodKg>=foodNeededKg){
            output = String.format ("%.0f kilos of food left.", Math.floor(foodKg-foodNeededKg));
        }else
            output = String.format("%.0f more kilos of food are needed.", Math.ceil(foodNeededKg-foodKg));


        System.out.println(output);
    }
}
