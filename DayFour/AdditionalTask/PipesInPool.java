package DayFour.AdditionalTask;

import java.util.Scanner;

public class PipesInPool {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int poolVolume = Integer.parseInt(scan.nextLine());
        int firstPipeDebit = Integer.parseInt(scan.nextLine());
        int secondPipeDebit = Integer.parseInt(scan.nextLine());
        double absence = Double.parseDouble(scan.nextLine());
        double litersFirstPipe = firstPipeDebit * absence;
        double litersSecondPipe = secondPipeDebit * absence;
        double waterIn = litersFirstPipe + litersSecondPipe;
        double percentWaterInTheSp = (waterIn / poolVolume) * 100;
        double percentWaterFirstPipe = (litersFirstPipe / waterIn) * 100;
        double percentWaterSecondPipe = (litersSecondPipe / waterIn) * 100;
        String output = "";
        if (poolVolume >= waterIn) {
            output = String.format("The pool is %.2f%% full. " +
                    "Pipe 1: %.2f%%. Pipe 2: %.2f%%.", percentWaterInTheSp, percentWaterFirstPipe, percentWaterSecondPipe);

        } else {
            output = String.format("For %.2f hours the pool overflows with %.2f liters.",absence, waterIn-poolVolume);
        }
        System.out.println(output);
        }

    }