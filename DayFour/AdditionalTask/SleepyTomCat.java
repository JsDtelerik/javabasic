package DayFour.AdditionalTask;

import java.util.Scanner;

public class SleepyTomCat {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int holidays = Integer.parseInt(scan.nextLine());
        int workdays = 365-holidays;
        int playTime = (workdays*63)+(holidays*127);
        int hoursPlay = Math.abs(30000-playTime)/60;
        int minutesPlay = Math.abs(30000-playTime)%60;
            String output = "";
        if(30000>playTime){
            output = String.format("Tom sleeps well%n" +
                    "%d hours and %d minutes less for play", hoursPlay, minutesPlay);
        }else{
            output = String.format("Tom will run away%n" +
                    "%d hours and %d minutes more for play", hoursPlay, minutesPlay);
        }

        System.out.println(output);

    }



}
