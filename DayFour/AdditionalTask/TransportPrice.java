package DayFour.AdditionalTask;

import java.util.Scanner;

public class TransportPrice {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int kilometers = Integer.parseInt(scan.nextLine());
        String  DayOrNight = scan.nextLine();
        double dayTimeTaxi = (kilometers*0.79)+0.70;
        double nightTimeTaxi = (kilometers*0.90)+0.70;
        double busExpenses = kilometers*0.09;
        double trainExpenses = kilometers*0.06;
        double result = 0.0;

        if (DayOrNight.equals("day") && kilometers<20){
            result = dayTimeTaxi;

        }else if (DayOrNight.equals("night") && kilometers<20){
            result = nightTimeTaxi;

        } else if (kilometers<100){
            result = busExpenses;

        }else
            result = trainExpenses;



        System.out.printf("%.2f", result);

    }
}
