package DayFour.AdditionalTask;

import java.util.Scanner;

public class repair {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String typeFlowers = scan.nextLine();
        int numberFlowers = Integer.parseInt(scan.nextLine());
        int budget = Integer.parseInt(scan.nextLine());


        Double totalPrice = 0.0;

        if (typeFlowers.equals("Roses")) {
            if (numberFlowers > 80) {
                totalPrice = (numberFlowers * 5.00)-((numberFlowers * 5.00)*0.10);
            } else {
                totalPrice = numberFlowers * 5.00;
            }
        } else if (typeFlowers.equals("Dahlias")) {
            if (numberFlowers > 90) {
                totalPrice = (numberFlowers*3.80)-((numberFlowers*3.80)*0.15);
            }

            else {
                totalPrice = numberFlowers * 3.80;
            }
        }else if (typeFlowers.equals("Tulips")) {
            if (numberFlowers > 80) {
                totalPrice = (numberFlowers * 2.80)-((numberFlowers * 2.80)*0.15);
            } else {
                totalPrice = numberFlowers * 2.80;
            }
        }else if (typeFlowers.equals("Narcissus")) {
            if (numberFlowers < 120) {
                totalPrice = (numberFlowers * 3.00)+((numberFlowers * 3.00)*0.15);
            } else {
                totalPrice = numberFlowers * 3.00;
            }
        }else if (typeFlowers.equals("Gladiolus")) {
            if (numberFlowers < 80) {
                totalPrice = (numberFlowers * 2.50)+((numberFlowers * 2.50)*0.20);
            } else {
                totalPrice = numberFlowers * 2.50;
            }
        }

        double finalAmount = Math.abs(totalPrice - budget);
        if (totalPrice <= budget) {
            System.out.printf("Hey, you have a great garden with %d %s and %.2f leva left.", numberFlowers, typeFlowers, finalAmount);
        } else {
            System.out.printf("Not enough money, you need %.2f leva more.", finalAmount);
        }
    }
}


