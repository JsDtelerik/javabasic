package DayFour;

import java.util.Scanner;

public class MetricConverter {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);


        double number = Double.parseDouble(scan.nextLine());
        String input = scan.nextLine();
        String output = scan.nextLine();

        double metersToMm = 1*1000;
        double centimetersToMm = 1*10;
        double centimetersToM = 1*100;

        if (input.equals("mm") && output.equals("m"))
            System.out.printf("%.3f", number/metersToMm);
        else if (input.equals("m") && output.equals("mm"))
            System.out.printf("%.3f", number*metersToMm);
        else if (input.equals("m") && output.equals("cm"))
            System.out.printf("%.3f", number*centimetersToM);
        else if (input.equals("cm") && output.equals("m"))
            System.out.printf("%.3f", number/centimetersToM);
        else if (input.equals("mm") && output.equals("cm"))
            System.out.printf("%.3f", number/centimetersToMm);
        else if (input.equals("cm") && output.equals("mm"))
            System.out.printf("%.3f", number*centimetersToMm);

    }

}
