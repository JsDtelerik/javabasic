package DayFour;

import java.util.Scanner;

public class SumSeconds {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int firstTime = Integer.parseInt(scan.nextLine());
        int secondTime = Integer.parseInt(scan.nextLine());
        int thirdTime = Integer.parseInt(scan.nextLine());
        int minutes = (firstTime+secondTime+thirdTime)/60;
        int seconds = (firstTime+secondTime+thirdTime)%60;

        if (seconds<10){
            System.out.printf("%d:0%d", minutes, seconds);
        }else
            System.out.printf("%d:%d", minutes, seconds);


    }
}
