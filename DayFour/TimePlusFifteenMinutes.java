package DayFour;

import java.util.Scanner;

public class TimePlusFifteenMinutes {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int hours = Integer.parseInt(scan.nextLine());
        int minutes = Integer.parseInt(scan.nextLine());
        int hoursToMin = (hours*60)+minutes+15;
        int hour = hoursToMin/60;
        int min = hoursToMin%60;
        if(hour<24 && min<10)
            System.out.printf("%d:0%d", hour, min);
        else if(hour<24 && min>=10)
            System.out.printf("%d:%d", hour, min);
        else if (hour==24 && min>=10)
            System.out.printf("0:%2d",min);
        else if (hour==24 && min<10)
            System.out.printf("0:0%d",min);
    }
}
