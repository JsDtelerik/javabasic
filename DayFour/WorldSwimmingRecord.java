package DayFour;

import java.util.Scanner;

public class WorldSwimmingRecord {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double record = Double.parseDouble(scan.nextLine());
        double distanceMeters = Double.parseDouble(scan.nextLine());
        double swimmingTimePerMeterSec = Double.parseDouble(scan.nextLine());
        double obstacleTime = (Math.floor(distanceMeters/15))*12.5;
        double swimmingTime = ((distanceMeters*swimmingTimePerMeterSec)+obstacleTime);
        if (record>swimmingTime) {
            System.out.printf("Yes, he succeeded! The new world record is %.2f seconds.", swimmingTime);
        }else
            System.out.printf("No, he failed! He was %.2f seconds slower.", swimmingTime-record);
    }
}
