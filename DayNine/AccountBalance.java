package DayNine;

import java.util.Scanner;

public class AccountBalance {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String input = scan.nextLine();
        double bankBalance = 0.0;
        while (!input.equals("NoMoreMoney")){
            double income = Double.parseDouble(input);
            if (income<0){
                System.out.println("Invalid operation!");
                break;
            }
            input = scan.nextLine();
            bankBalance+=income;
            System.out.printf("Increase: %.02f%n", income);

        }

        System.out.printf("Total: %.02f", bankBalance);

    }
}
