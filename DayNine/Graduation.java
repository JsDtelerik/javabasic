package DayNine;

import java.util.Scanner;

public class Graduation {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String name = scan.nextLine();
        int cls = 1;
        int fails = 0;
        double average = 0.0;
        while (cls<=12){
            double grade = Double.parseDouble(scan.nextLine());
            if (grade>=4.00){
                average+=grade;
                cls++;
                continue;
            }
            fails++;
            average+=grade;
            if (fails>1){
                break;
            }


        }
        if (fails>1){
            System.out.printf("%s has been excluded at %d grade", name, cls);
        }else{
            System.out.printf("%s graduated. Average grade: %.02f", name, average/12);
        }


    }
}
