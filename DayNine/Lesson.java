package DayNine;

import java.util.Scanner;

public class Lesson {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int numDays = Integer.parseInt(scanner.nextLine());
        double total_quantityFood = Double.parseDouble(scanner.nextLine());

        double biscuits_g = 0.0;
        double total_food_eaten_by_dog = 0.0;
        double total_food_eaten_by_cat = 0.0;
        double biscuitsTotalPeriod = 0.0;

        for (int i = 1; i <= numDays; i++) {
            int day_dog_food_g = Integer.parseInt(scanner.nextLine());
            int day_cat_food_g = Integer.parseInt(scanner.nextLine());

            if (i % 3 ==0)
            {
                biscuits_g =  (day_cat_food_g + day_dog_food_g) * 0.1;
                biscuitsTotalPeriod +=biscuits_g;
            }
            total_food_eaten_by_dog += day_dog_food_g;
            total_food_eaten_by_cat += day_cat_food_g;

        }
        double total_food_eaten = total_food_eaten_by_dog + total_food_eaten_by_cat;
        double total_food_eaten_percents = 100.0 * total_food_eaten / total_quantityFood;
        double total_food_eaten_by_dog_percents = 100.0 * total_food_eaten_by_dog / total_food_eaten;
        double total_food_eaten_by_cat_percents = 100.0 * total_food_eaten_by_cat / total_food_eaten;


        System.out.printf("Total eaten biscuits: %.0fgr.%n", biscuitsTotalPeriod);
        System.out.printf("%.02f%% of the food has been eaten.%n", total_food_eaten_percents);
        System.out.printf("%.02f%% eaten from the dog.%n",total_food_eaten_by_dog_percents );
        System.out.printf("%.02f%% eaten from the cat.%n",total_food_eaten_by_cat_percents );

    }
}


