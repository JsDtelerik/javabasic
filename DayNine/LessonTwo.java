package DayNine;

import java.util.Scanner;

public class LessonTwo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String city = scanner.nextLine().toLowerCase();
        double s = Double.parseDouble(scanner.nextLine());

        double commission = 0.0;

        if (s <= 500 && s >= 0) {
            switch (city) {
                case "sofia":
                    commission = s * 0.05;
                    break;
                case "varna":
                    commission = s * 0.045;
                    break;
                case "plovdiv":
                    commission = s * 0.055;
                    break;
                default:
                    System.out.println("error");
                    break;
            }

        } else if (s > 500 && s <= 1000) {
            switch (city) {
                case "sofia":
                    commission = s * 0.07;
                    break;
                case "varna":
                    commission = s * 0.075;
                    break;
                case "plovdiv":
                    commission = s * 0.08;
                    break;
                default:
                    System.out.println("error");
                    break;
            }
        } else if (s > 1000 && s <= 10000) {
            switch (city) {
                case "sofia":
                    commission = s * 0.08;
                    break;
                case "varna":
                    commission = s * 0.1;
                    break;
                case "plovdiv":
                    commission = s * 0.12;
                    break;

                default:
                    System.out.println("error");
                    break;

            }


        } else if (s > 10000) {
            switch (city) {
                case "sofia":
                    commission = s * 0.12;
                    break;
                case "varna":
                    commission = s * 0.13;
                    break;
                case "plovdiv":
                    commission = s * 0.145;
                    break;
                default:
                    System.out.println("error");
                    break;
            }
        } else {
            System.out.println("error");


        }
        if (commission>0){
            System.out.printf("%.2f", commission);
        }

    }
}

