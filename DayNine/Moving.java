package DayNine;

import java.util.Scanner;

public class Moving {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int w = Integer.parseInt(scan.nextLine());
        int l = Integer.parseInt(scan.nextLine());
        int h = Integer.parseInt(scan.nextLine());
        int volume = w*l*h;

        String input = scan.nextLine();
        int boxes = 0;
        while (!input.equals("Done")){
            int box = Integer.parseInt(input);
            boxes+=box;
            if (boxes>volume){
                break;
            }
            input = scan.nextLine();

        }

        if(volume>boxes){
            System.out.printf("%d Cubic meters left.", Math.abs(volume-boxes));
        }else{
            System.out.printf("No more free space! You need %d Cubic meters more.", Math.abs(volume-boxes));
        }
    }
}
