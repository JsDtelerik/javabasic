package DayOnw.Exercises;

import java.util.Scanner;

public class CircleAreaAndPerimeter {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        double radius = Double.parseDouble(scan.nextLine());

        double s = Math.PI *(radius*radius);
        double p = (2*Math.PI)*radius;
        System.out.printf("%.2f%n%.2f", s, p);

    }
}
