package DayOnw.Exercises;

import java.util.Scanner;

public class Fishland {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double mackerelPricePerKg = Double.parseDouble(scan.nextLine());
        double spratPricePerKg = Double.parseDouble(scan.nextLine());
        double beltedBonitoKg = Double.parseDouble(scan.nextLine());
        double scadKg = Double.parseDouble(scan.nextLine());
        int musselsKg = Integer.parseInt(scan.nextLine());
        double beltedBonitoPricePerKg = mackerelPricePerKg+(mackerelPricePerKg*0.6);
        double scadPricePerKg = spratPricePerKg+(spratPricePerKg*0.8);
        double totalPrice = (musselsKg*7.5)+(scadKg*scadPricePerKg)+(beltedBonitoKg*beltedBonitoPricePerKg);
        System.out.printf("%.2f", totalPrice);

    }
}

