package DayOnw.Exercises;

import java.nio.DoubleBuffer;
import java.util.Scanner;

public class HousePainting {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        //⦁	x – височината на къщата – реално число в интервала [2...100]
        //⦁	y – дължината на страничната стена – реално число в интервала [2...100]
        //⦁	h – височината на триъгълната стена на прокрива – реално число в интервала [2...100]

        double houseHeight = Double.parseDouble(scan.nextLine());
        double houseLength = Double.parseDouble(scan.nextLine());
        double triangleHeight = Double.parseDouble(scan.nextLine());
        double sides = ((houseHeight*houseLength)*2)-((1.5*1.5)*2);
        double front = ((houseHeight*houseHeight)*2) - (1.2*2);
        double roof = ((houseHeight*houseLength)*2);
        double roofTriangle = ((houseHeight*triangleHeight)/2)*2;

        double paintForTheWalls = (sides+front)/3.4;
        double paintForTheRoof = (roof+roofTriangle)/4.3;
        System.out.printf("%.2f%n%.2f", paintForTheWalls, paintForTheRoof);

    }
}
