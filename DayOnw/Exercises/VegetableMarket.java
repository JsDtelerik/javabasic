package DayOnw.Exercises;

import java.util.Scanner;

public class VegetableMarket {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        double VegetablesPriceKg = Double.parseDouble(scan.nextLine());

        double FruitsPriceKg = Double.parseDouble(scan.nextLine());

        int VegetablesKg = Integer.parseInt(scan.nextLine());

        int FruitsKg = Integer.parseInt(scan.nextLine());

        double vegatablesTotalPrice = VegetablesPriceKg*VegetablesKg;
        double fruitsTotalPrice = FruitsPriceKg*FruitsKg;

        double incomes = (fruitsTotalPrice+vegatablesTotalPrice) / 1.94;

        System.out.printf("%.2f", incomes);
    }
}
