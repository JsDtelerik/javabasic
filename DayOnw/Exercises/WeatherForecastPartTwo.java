package DayOnw.Exercises;

import java.util.Scanner;

public class WeatherForecastPartTwo {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        double degrees = Double.parseDouble(scan.nextLine());
        if (degrees >= 35.01)
            System.out.println("unknown");
        else if (degrees >= 26.00)
            System.out.println("Hot");
        else if (degrees >= 20.1)
           System.out.println("Warm");
        else if (degrees >= 15.00)
            System.out.println("Mild");
        else if (degrees >= 12.00)
            System.out.println("Cool");
        else if (degrees >= 5.00)
            System.out.println("Cold");
        else
            System.out.println("unknown");

    }
}
