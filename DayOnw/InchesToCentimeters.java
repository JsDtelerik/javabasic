package DayOnw;

import java.util.Scanner;

public class InchesToCentimeters {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        double inches1 = Double.parseDouble(scan.nextLine());
        double centemetres1 = inches1 * 2.54;
        System.out.println(centemetres1);
    }
}
