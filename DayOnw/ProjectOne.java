package DayOnw;

import java.util.Scanner;

public class ProjectOne {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("What is the name of the architect?");
        String name = scan.nextLine();
        int hours = 3;
        System.out.println("How many project the architect have to complete?");
        int projects = Integer.parseInt(scan.nextLine());
        int  whours  = hours * projects;
        System.out.println("The architect " +name + " will need " + whours + " hours to complete " + projects + " project/s.");

    }
}
