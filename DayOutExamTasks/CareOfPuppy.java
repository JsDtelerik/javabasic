package DayOutExamTasks;

import java.util.Scanner;

public class CareOfPuppy {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int boughtFoodKg = Integer.parseInt(scan.nextLine());
        String input = scan.nextLine();
        int foodGr = boughtFoodKg*1000;
        int totalEatenFood = 0;
        while (!input.equals("Adopted")){
            int eatenFoodGr = Integer.parseInt(input);
            totalEatenFood +=eatenFoodGr;
            input = scan.nextLine();
        }
        String result = "";
        if (totalEatenFood<=foodGr){
            result = String.format("Food is enough! Leftovers: %d grams.", foodGr-totalEatenFood);
        }else{
            result = String.format("Food is not enough. You need %d grams more.", totalEatenFood-foodGr);
        }
        System.out.println(result);
    }

}
