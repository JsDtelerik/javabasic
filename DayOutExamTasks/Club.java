package DayOutExamTasks;

import java.util.Scanner;

public class Club {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double targetIncome = Double.parseDouble(scan.nextLine());
        String input = scan.nextLine();
        double income = 0.0;
        double cocktailRealPrice = 0.0;
        double cocktailDiscountPrice = 0.0;
        boolean targetAcquired = false;
        while (!input.equals("Party!")){
            int cocktailsNum = Integer.parseInt(scan.nextLine());
            int cocktailsPrice = input.length();
            cocktailRealPrice = cocktailsPrice*cocktailsNum;
            if(cocktailRealPrice%2==0){
                income +=cocktailRealPrice;
            }else{
                cocktailDiscountPrice =cocktailRealPrice*0.75;
                income+=cocktailDiscountPrice;
            }
            if (income>=targetIncome){
                targetAcquired = true;
                break;
            }
            input = scan.nextLine();
        }
        if (targetAcquired){
            System.out.printf("Target acquired.%n" +
                    "Club income - %.02f leva.", income);

        }else {
            System.out.printf("We need %.02f leva more.%n" +
                    "Club income - %.02f leva.", targetIncome-income, income);
        }
    }
}
