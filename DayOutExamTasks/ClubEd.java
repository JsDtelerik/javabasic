package DayOutExamTasks;

import java.util.Scanner;

public class ClubEd {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double income = Double.parseDouble(scanner.nextLine());
        double priceForCocktail = 0.0;
        boolean isParty = false;
        double total = 0.0;
        boolean targetAcquired = false;
        while (total <= income){
            String cocktail = scanner.nextLine();
            if(cocktail.equals("Party!")){
                isParty = true;
                break;
            }
            int symbol = cocktail.length();
            int numCocktails = Integer.parseInt(scanner.nextLine());
            priceForCocktail = numCocktails * symbol;
            if(priceForCocktail % 2 != 0){
                priceForCocktail *= 0.75;
            }
            total += priceForCocktail;
            if (total>=income){
                targetAcquired=true;
                break;
            }
        }
        if(isParty) {
            System.out.printf("We need %.02f leva more.%n",income - total);
        }else{
            System.out.printf("Target acquired.%n");
        }
        System.out.printf("Club income - %.02f leva.",total);
    }
}



