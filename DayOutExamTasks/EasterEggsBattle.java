package DayOutExamTasks;

import java.util.Scanner;

public class EasterEggsBattle {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int playerOneEggs = Integer.parseInt(scan.nextLine());
        int playerTwoEggs = Integer.parseInt(scan.nextLine());
        String input = scan.nextLine();
        String result = "";
        while (!input.equals("End of battle")){
            switch (input){
                case "one": playerTwoEggs--; break;
                case "two": playerOneEggs--; break;
            }
            if (playerOneEggs<= 0){
                result = String.format("Player one is out of eggs. Player two has %d eggs left.", playerTwoEggs);
                break;
            }else if (playerTwoEggs<=0){
                result = String.format("Player two is out of eggs. Player one has %d eggs left.", playerOneEggs);
                break;
            }
            input = scan.nextLine();
            if (input.equals("End of battle"))
                result = String.format("Player one has %d eggs left.%n" +
                        "Player two has %d eggs left.", playerOneEggs, playerTwoEggs);
        }
        System.out.println(result);
    }
}
