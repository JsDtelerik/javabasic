package DayOutExamTasks;

import java.util.Scanner;

public class EasterShop {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int stockEggs = Integer.parseInt(scan.nextLine());
        String input = scan.nextLine();
        String result = "";
        int soldEggs = 0;
        while (!input.equals("Close")){
            int eggs = Integer.parseInt(scan.nextLine());
            if (input.equals("Buy")){
                if (stockEggs>=eggs){
                        stockEggs -=eggs;
                        soldEggs+=eggs;
                    } else {
                        result = String.format("Not enough eggs in store!%n" +
                                "You can buy only %d.", stockEggs); break;
                    }
            }else if (input.equals("Fill")){
                    stockEggs +=eggs;
            }
            input = scan.nextLine();
        }
        if (input.equals("Close")){
            result = String.format("Store is closed!%n" +
                    "%d eggs sold.", soldEggs);
        }
        System.out.println(result);
    }
}
