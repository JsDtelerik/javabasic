package DayOutExamTasks;

import java.util.Scanner;

public class GraduationPart2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        int failure = 0;
        boolean hasFailed = false;
        int grade = 1;
        double averageScore;
        int lastGrade = 0;
        double sumOfMarks = 0.0;

        while (grade <= 12) {
            double schoolYearMark = Double.parseDouble(scanner.nextLine());

            if (schoolYearMark < 4.0) {
                failure += 1;
            }
            if (failure > 1) {
                hasFailed = true;
                lastGrade = grade-1;
                break;
            }
            grade++;
            sumOfMarks += schoolYearMark;
        }
        String output;
        if (hasFailed) {
            output = String.format("%s has been excluded at %d grade", name, lastGrade);
        } else {
            averageScore = sumOfMarks / 12;
            output = String.format("%s graduated. Average grade: %.2f", name, averageScore);
        }
        System.out.println(output);
    }
}

