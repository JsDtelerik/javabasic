package DayOutExamTasks.Practice;

import java.util.Scanner;

public class Darts {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String playerName = scan.nextLine();
        String sector = scan.nextLine();
        int currentPoints = 301;
        int successfulShots = 0;
        int unsuccessfulShoots = 0;
        int pointPerLeg = 0;
        boolean youWon = false;
        while (!sector.equals("Retire")){
            int points = Integer.parseInt(scan.nextLine());
                switch (sector){
                    case "Single":
                        pointPerLeg = points;
                        if (pointPerLeg >= currentPoints){
                            unsuccessfulShoots++;
                            sector = scan.nextLine();
                            continue;
                        }else{
                            currentPoints -= points;
                            successfulShots++;
                        }
                         break;
                    case "Double":
                        pointPerLeg = points*2;
                        if (pointPerLeg >= currentPoints){
                            unsuccessfulShoots++;
                            sector = scan.nextLine();
                            continue;
                        }else{
                            currentPoints -= points*2;
                            successfulShots++;
                        }
                        break;
                    case "Triple":
                        pointPerLeg = 3*points;
                        if (pointPerLeg >= currentPoints){
                            unsuccessfulShoots++;
                            sector = scan.nextLine();
                            continue;
                        }else{
                            currentPoints -= points*3;
                            successfulShots++;
                        }
                        break;
                }
            if (currentPoints == 0){
                youWon = true;
                break;
            }
            sector = scan.nextLine();
        }
        if (youWon){
            System.out.printf("%s won the leg with %d shots.", playerName, successfulShots);
        }else{
            System.out.printf("%s retired after %d unsuccessful shots.", playerName, unsuccessfulShoots);
        }
    }
}
