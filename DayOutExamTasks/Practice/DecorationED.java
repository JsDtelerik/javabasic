package DayOutExamTasks.Practice;

import java.util.Scanner;

public class DecorationED {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = Integer.parseInt(scanner.nextLine());
        String command = scanner.nextLine();

        double ultimatePrice = 0.0;
        double averagePrice = 0.0;

        for (int i = 1; i <= n; i++) {
            int numberPurchases = 0;
            double price = 0.0;
            double totalPrice = 0.0;
            while (!command.equals("Finish")) {
                String purchase = command;
                switch (purchase) {
                    case "basket":
                        price = 1.50;
                        break;
                    case "wreath":
                        price = 3.80;
                        break;
                    case "chocolate bunny":
                        price = 7.00;
                        break;
                }
                numberPurchases++;
                totalPrice += price;
                ultimatePrice += totalPrice;
                command = scanner.nextLine();
            }
            if (numberPurchases % 2 == 0) {
                totalPrice -= 0.2 * totalPrice;
            }
            System.out.printf("You purchased %d items for %.2f leva.%n", numberPurchases, totalPrice);
            command = scanner.nextLine();
        }
        averagePrice = ultimatePrice / n;
        System.out.printf("Average bill per client is: %.2f leva.", averagePrice);
    }
}


