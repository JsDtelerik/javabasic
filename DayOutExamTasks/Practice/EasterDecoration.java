package DayOutExamTasks.Practice;

import java.util.Scanner;

public class EasterDecoration {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int customersNumber = Integer.parseInt(scan.nextLine());
        double totalBill = 0.0;
        for (int i = 0; i <customersNumber ; i++) {
            String purchase = scan.nextLine();
            int currentCustomerItems = 0;
            double customersBill = 0.0;
            while (!purchase.equals("Finish")) {
                switch (purchase) {
                    case "basket":
                        customersBill += 1.5;
                        currentCustomerItems++;
                        break;
                    case "wreath":
                        customersBill += 3.8;
                        currentCustomerItems++;
                        break;
                    case "chocolate bunny":
                        customersBill += 7;
                        currentCustomerItems++;
                        break;
                }
                purchase = scan.nextLine();
            }
            if (currentCustomerItems % 2 == 0) {
                customersBill *= 0.8;

            }
            System.out.printf("You purchased %d items for %.02f leva.%n", currentCustomerItems, customersBill);
            totalBill += customersBill;

            }
            System.out.printf("Average bill per client is: %.02f leva.", totalBill/customersNumber);
    }
}
