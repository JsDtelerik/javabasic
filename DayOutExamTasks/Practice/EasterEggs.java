package DayOutExamTasks.Practice;

import java.util.Scanner;

public class EasterEggs {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int coloredEggs = Integer.parseInt(scan.nextLine());
        String color = scan.nextLine();
        int redEggsCount = 0;
        int orangeEggsCount =0;
        int blueEggsCount =0;
        int greenEggsCount =0;
        int maxEggs = Integer.MIN_VALUE;
        String keepColor = "";
        for (int i = 1; i <=coloredEggs ; i++) {
            switch (color){
                case "red": redEggsCount++;
                    if (maxEggs<redEggsCount) {
                        maxEggs = redEggsCount;
                        keepColor = color;
                    }
                break;
                case "orange": orangeEggsCount++;
                    if (maxEggs<orangeEggsCount) {
                        maxEggs = orangeEggsCount;
                        keepColor = color;
                    }
                break;
                case "blue": blueEggsCount++;
                    if (maxEggs<blueEggsCount) {
                    maxEggs = blueEggsCount;
                        keepColor = color;
                }
                break;
                case "green": greenEggsCount++;
                    if (maxEggs<greenEggsCount) {
                        maxEggs = greenEggsCount;
                        keepColor = color;
                    }
                break;
            }
            if (i == coloredEggs){
                break;
            }
          color = scan.nextLine();
        }
        System.out.printf("Red eggs: %d%n" +
                "Orange eggs: %d%n" +
                "Blue eggs: %d%n" +
                "Green eggs: %d%n", redEggsCount, orangeEggsCount, blueEggsCount, greenEggsCount);
        System.out.printf("Max eggs: %d -> %s", maxEggs, keepColor);

    }
}
