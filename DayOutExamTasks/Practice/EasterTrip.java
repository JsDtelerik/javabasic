package DayOutExamTasks.Practice;

import java.util.Scanner;

public class EasterTrip {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String destination = scan.nextLine();
        String period = scan.nextLine();
        int overnights = Integer.parseInt(scan.nextLine());
        double overnightsCost = 0.0;
        switch (destination){
            case "France":
                if (period.equals("21-23")){
                    overnightsCost = overnights*30;
                }else if (period.equals("24-27")){
                    overnightsCost = overnights*35;
                }else if (period.equals("28-31")){
                    overnightsCost = overnights*40;
                }
                    break;
            case "Italy":
                if (period.equals("21-23")){
                    overnightsCost = overnights*28;
                }else if (period.equals("24-27")){
                    overnightsCost = overnights*32;
                }else if (period.equals("28-31")){
                    overnightsCost = overnights*39;
                }
                break;
            case "Germany":
                if (period.equals("21-23")){
                    overnightsCost = overnights*32;
                }else if (period.equals("24-27")){
                    overnightsCost = overnights*37;
                }else if (period.equals("28-31")){
                    overnightsCost = overnights*43;
                }
                break;

        }

        System.out.printf("Easter trip to %s : %.02f leva.", destination, overnightsCost);
    }
}
