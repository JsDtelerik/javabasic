package DayOutExamTasks.Practice;

import java.util.Scanner;

public class MovieDay {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int shootingTime = Integer.parseInt(scan.nextLine());
        int scenes = Integer.parseInt(scan.nextLine());
        int sceneTime = Integer.parseInt(scan.nextLine());
        double terrainPreparation = shootingTime*0.15;
        int allScenesTime = scenes*sceneTime;
        double timeNeeded = terrainPreparation+allScenesTime;
        if (shootingTime>=timeNeeded){
            System.out.printf("You managed to finish the movie on time! You have %.0f minutes left!",shootingTime-timeNeeded);
        }else{
            System.out.printf("Time is up! To complete the movie you need %.0f minutes.", timeNeeded-shootingTime);
        }

    }
}
