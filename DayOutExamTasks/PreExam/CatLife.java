package DayOutExamTasks.PreExam;

import java.util.Scanner;

public class CatLife {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String catBreed = scan.nextLine();
        String catGender = scan.nextLine();
        int catLife = 0;
        String result = "";
        switch (catBreed){
            case "British Shorthair":
            if (catGender.equals("m")){
                catLife = (13*12)/6;
                result = String.format("%d cat months", catLife);
            }else{
                catLife = (14*12)/6;
                result = String.format("%d cat months", catLife);
            }
                break;
            case "Siamese":
                if (catGender.equals("m")){
                    catLife = (15*12)/6;
                    result = String.format("%d cat months", catLife);
                }else{
                    catLife = (16*12)/6;
                    result = String.format("%d cat months", catLife);
                }
                break;
            case "Persian":
                if (catGender.equals("m")){
                    catLife = (14*12)/6;
                    result = String.format("%d cat months", catLife);
                }else{
                    catLife = (15*12)/6;
                    result = String.format("%d cat months", catLife);
                }
                break;
            case "Ragdoll":
                if (catGender.equals("m")){
                    catLife = (16*12)/6;
                    result = String.format("%d cat months", catLife);
                }else{
                    catLife = (17*12)/6;
                    result = String.format("%d cat months", catLife);
                }
                break;
            case "American Shorthair":
                if (catGender.equals("m")){
                    catLife = (12*12)/6;
                    result = String.format("%d cat months", catLife);
                }else{
                    catLife = (13*12)/6;
                    result = String.format("%d cat months", catLife);
                }
                break;
            case "Siberian":
                if (catGender.equals("m")){
                    catLife = (11*12)/6;
                    result = String.format("%d cat months", catLife);
                }else{
                    catLife = (12*12)/6;
                    result = String.format("%d cat months", catLife);
                }
                break;
            default:
                result = String.format("%s is invalid cat!", catBreed); break;
        }
        System.out.print(result);
    }
}
