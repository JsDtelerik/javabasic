package DayOutExamTasks.PreExam;

import java.util.Scanner;

public class GoldMine {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int locations = Integer.parseInt(scan.nextLine());
        for (int i = 1; i <=locations ; i++) {
            double expectedReturnOfGold = Double.parseDouble(scan.nextLine());
            int diggingDays = Integer.parseInt(scan.nextLine());
            double averageGoldPerDay = 0.0;
            for (int j = 1; j <=diggingDays ; j++) {
                double goldPerDay = Double.parseDouble(scan.nextLine());
                averageGoldPerDay += goldPerDay/diggingDays;
            }
            if (expectedReturnOfGold<=averageGoldPerDay){
                System.out.printf("Good job! Average gold per day: %.02f.%n", averageGoldPerDay);
            }else{
                System.out.printf("You need %.02f gold.%n", expectedReturnOfGold-averageGoldPerDay);
            }

        }
    }
}
