package DayOutExamTasks.PreExam;

import java.util.Scanner;

public class PassengersPerFlight {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int airCompanies = Integer.parseInt(scan.nextLine());
        int maxAveragePassengers = Integer.MIN_VALUE;
        String keepCompanyName = "";
        for (int i = 0; i < airCompanies; i++) {
            String companyName = scan.nextLine();
            String currentCompany = companyName;
            int currentCompanyAllPassengers = 0;
            int currentCompanyFlights = 0;
            while (!companyName.equals("Finish")){
                companyName = scan.nextLine();
                if (!companyName.equals("Finish")) {
                    int passengersPerFlight = Integer.parseInt(companyName);
                    currentCompanyAllPassengers += passengersPerFlight;
                    currentCompanyFlights++;
                }
            }
            int averagePassengerPerCompany = currentCompanyAllPassengers/currentCompanyFlights;
            System.out.printf("%s: %d passengers.%n", currentCompany, averagePassengerPerCompany);
            if (maxAveragePassengers<averagePassengerPerCompany){
                maxAveragePassengers = averagePassengerPerCompany;
                 keepCompanyName = currentCompany;
            }

        }
        System.out.printf("%s has most passengers per flight: %d", keepCompanyName, maxAveragePassengers);

    }
}
