package DayOutExamTasks.PreExam;

import java.util.Scanner;

public class ProgrammingBook {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double pricePerSheet = Double.parseDouble(scan.nextLine());
        double pricePerBookCover = Double.parseDouble(scan.nextLine());
        int discountPercent = Integer.parseInt(scan.nextLine());
        double designerFee = Double.parseDouble(scan.nextLine());
        int teamMembersHelpPercent = Integer.parseInt(scan.nextLine());

        double bookCost = ((pricePerSheet*899)+(pricePerBookCover*2));
        double bookdiscount = bookCost-(bookCost*discountPercent/100);
        double expenditure = bookdiscount+designerFee;
        double finalBookCost = expenditure - (expenditure*teamMembersHelpPercent/100);

        System.out.printf("Avtonom should pay %.02f BGN.", finalBookCost);

    }
}
