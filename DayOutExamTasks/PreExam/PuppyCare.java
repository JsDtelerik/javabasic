package DayOutExamTasks.PreExam;

import java.util.Scanner;

public class PuppyCare {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int boughtFoodKg = Integer.parseInt(scan.nextLine());
        String input = scan.nextLine();
        int boughtFoodGr =boughtFoodKg*1000;
        int eatenFood = 0;
        while (!input.equals("Adopted")){
            int eatenFoodPerMeal = Integer.parseInt(input);
            eatenFood += eatenFoodPerMeal;
            input = scan.nextLine();
        }
        if (eatenFood<=boughtFoodGr){
            System.out.printf("Food is enough! Leftovers: %d grams.", boughtFoodGr-eatenFood);
        }else{
            System.out.printf("Food is not enough. You need %d grams more.", eatenFood-boughtFoodGr);
        }


    }
}
