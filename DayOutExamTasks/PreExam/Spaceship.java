package DayOutExamTasks.PreExam;

import java.util.Scanner;

public class Spaceship {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double spaceshipWight = Double.parseDouble(scan.nextLine());
        double spaceShipLength = Double.parseDouble(scan.nextLine());
        double spaceShipHeight = Double.parseDouble(scan.nextLine());
        double averageAstronautsHeight = Double.parseDouble(scan.nextLine());

        double spaceshipVolume = spaceshipWight*spaceShipLength*spaceShipHeight;
        double spaceshipRoomVolume = (averageAstronautsHeight+0.40)*2*2;
        double availableRoomForAstronauts = Math.floor(spaceshipVolume/spaceshipRoomVolume);
        if (availableRoomForAstronauts>=3 && availableRoomForAstronauts<=10){
            System.out.printf("The spacecraft holds %.0f astronauts.", availableRoomForAstronauts);
        }else if (availableRoomForAstronauts<3){
            System.out.printf("The spacecraft is too small.");
        }else{
            System.out.printf("The spacecraft is too big.");
        }
    }
}
