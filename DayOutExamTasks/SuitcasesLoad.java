package DayOutExamTasks;

import java.util.Scanner;

public class SuitcasesLoad {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String input = scan.nextLine();
        double  capacity = 0.0;
        int suitcaseCounter = 0;
        boolean noMoreSpace = false;
        while (!input.equals("End")){
            double suitcase = Double.parseDouble(input);
            if  (suitcaseCounter == 0){
                    capacity = suitcase;
                    suitcaseCounter++;
                    input = scan.nextLine();
                    continue;
                }else if (suitcaseCounter %3 == 0){
                suitcase = suitcase*1.10;

                }
            if (capacity<suitcase) {
                noMoreSpace = true;
                break;
            }
            capacity -=suitcase;
            input = scan.nextLine();
            suitcaseCounter++;

        }
        if (noMoreSpace){
            System.out.printf("No more space!%n" +
                    "Statistic: %d suitcases loaded.", suitcaseCounter-1);
        }else{
            System.out.printf("Congratulations! All suitcases are loaded!%n" +
                    "Statistic: %d suitcases loaded.", suitcaseCounter-1);
        }
    }
}
