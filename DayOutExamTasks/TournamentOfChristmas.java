package DayOutExamTasks;

import java.util.Scanner;

public class TournamentOfChristmas {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int daysOfPlay = Integer.parseInt(scan.nextLine());
        String sportGames = scan.nextLine();
        double moneyWon = 0.0;
        int dayWon = 0;
        for (int i = 1; i <=daysOfPlay ; i++) {
                int wonGamesPerDay = 0;
                int lostGamesPerDay = 0;
                int dayWonCount = 0;
                dayWon +=dayWonCount;
                double moneyWonPerDay=0;

            while (!sportGames.equals("Finish")){
                String winOrLose = scan.nextLine();
                    switch (winOrLose){
                        case "win":  wonGamesPerDay++; break;
                        case "lose": lostGamesPerDay++; break;
                    }
                sportGames = scan.nextLine();
            }
            if (wonGamesPerDay>lostGamesPerDay){
                dayWon++;
                moneyWonPerDay = (wonGamesPerDay*20)*1.1;
            }else{
                moneyWonPerDay = wonGamesPerDay*20;
            }
            moneyWon += moneyWonPerDay;
            if (i == daysOfPlay){
                break;
            }
            sportGames = scan.nextLine();
        }
        int dayLost = daysOfPlay-dayWon;
        if (dayWon > dayLost){
            moneyWon *= 1.2;
            System.out.printf("You won the tournament! Total raised money: %.02f", moneyWon);
        }else{
            System.out.printf("You lost the tournament! Total raised money: %.02f", moneyWon);
        }
    }
}
