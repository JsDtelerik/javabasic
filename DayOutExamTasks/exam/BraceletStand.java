package DayOutExamTasks.exam;

import java.util.Scanner;

public class BraceletStand {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double pocketMoneyPerDay = Double.parseDouble(scan.nextLine());
        double salesIncomesPerDay = Double.parseDouble(scan.nextLine());
        double expenses = Double.parseDouble(scan.nextLine());
        double giftPrice = Double.parseDouble(scan.nextLine());
        double spentMoney = ((pocketMoneyPerDay*5)+(salesIncomesPerDay*5))-expenses;
        if (giftPrice<=spentMoney){
            System.out.printf("Profit: %.02f BGN, the gift has been purchased.", spentMoney);
        }else{
            System.out.printf("Insufficient money: %.02f BGN.", giftPrice-spentMoney);
        }
    }
}
