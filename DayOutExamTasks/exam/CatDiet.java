package DayOutExamTasks.exam;

import java.util.Scanner;

public class CatDiet {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int fatPercent = Integer.parseInt(scan.nextLine());
        int proteinPercent = Integer.parseInt(scan.nextLine());
        int carbohydratePercent = Integer.parseInt(scan.nextLine());
        int calories = Integer.parseInt(scan.nextLine());
        int waterPercent = Integer.parseInt(scan.nextLine());
        double fatGrams = (1.0*fatPercent*calories/100)/9;
        double proteinGrams = (1.0*proteinPercent*calories/100)/4;
        double carbohydrateGrams = (1.0*carbohydratePercent*calories/100)/4;
        double caloriesGrams = fatGrams+proteinGrams+carbohydrateGrams;
        double caloriesGr = ((calories/caloriesGrams)*(100-waterPercent))/100;
        System.out.printf("%.04f", caloriesGr);

    }
}
