package DayOutExamTasks.exam;

import java.util.Scanner;

public class HairSalon {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int dayTarget = Integer.parseInt(scan.nextLine());
        String input = scan.nextLine();
        int princePerProcedure = 0;
        int collectedMoney = 0;
        boolean targetReached = false;
        while (!input.equals("closed")){
            String procedureType = scan.nextLine();
            if (input.equals("haircut")){
                switch (procedureType){
                    case "mens": princePerProcedure = 15; break;
                    case "ladies": princePerProcedure = 20; break;
                    case "kids": princePerProcedure = 10; break;
                }
            }else if (input.equals("color")){
                switch (procedureType){
                    case "touch up": princePerProcedure = 20; break;
                    case "full color": princePerProcedure = 30; break;
                }
            }
            collectedMoney +=princePerProcedure;
            if (collectedMoney>=dayTarget){
                targetReached = true;
                break;
            }
            input = scan.nextLine();
        }
        if (targetReached){
            System.out.printf("You have reached your target for the day!%n");
        }else{
            System.out.printf("Target not reached! You need %dlv. more.%n", dayTarget-collectedMoney);
        }
        System.out.printf("Earned money: %dlv.", collectedMoney);
    }
}
