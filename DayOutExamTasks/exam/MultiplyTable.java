package DayOutExamTasks.exam;

import java.util.Scanner;

public class MultiplyTable {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = Integer.parseInt(scan.nextLine());
        int n3 = n%10;
        n = n/10;
        int n2 = n%10;
        n = n/10;
        int n1 = n%10;
        for (int i = 1; i <=n3; i++) {
            for (int j = 1; j <=n2 ; j++) {
                for (int k = 1; k <=n1 ; k++) {
                    System.out.printf("%d * %d * %d = %d;%n", i, j, k, i*j*k);
                }
            }
        }
    }
}
