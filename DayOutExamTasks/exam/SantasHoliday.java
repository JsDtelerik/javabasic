package DayOutExamTasks.exam;

import java.util.Scanner;

public class SantasHoliday {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int accommodationDays = Integer.parseInt(scan.nextLine());
        String room = scan.nextLine();
        String evaluation = scan.nextLine();
        int overnights = accommodationDays-1;
        double price = 0;
        switch (room){
            case "room for one person": price = 18*overnights; break;
            case "apartment":
                if (accommodationDays<10){
                    price = (25*overnights)*0.7;
                }else if (accommodationDays>=10 && accommodationDays<=15){
                    price = (overnights*25)*0.65;
                }else if (accommodationDays>15){
                    price = (overnights*25)*0.50;
                }
                break;
            case "president apartment":
                if (accommodationDays<10){
                    price = (35*overnights)*0.9;
                }else if (accommodationDays>=10 && accommodationDays<=15){
                    price = (overnights*35)*0.85;
                }else if (accommodationDays>15){
                    price = (overnights*35)*0.80;
                }
                break;
        }

        if (evaluation.equals("positive")){
            price *=1.25;
        }else if (evaluation.equals("negative")){
            price *=0.9;
        }
        System.out.printf("%.02f", price);
    }
}
