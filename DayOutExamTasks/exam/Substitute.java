package DayOutExamTasks.exam;

import java.util.Scanner;

public class Substitute {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n11 = Integer.parseInt(scan.nextLine());
        int n12 = Integer.parseInt(scan.nextLine());
        int n21 = Integer.parseInt(scan.nextLine());
        int n22 = Integer.parseInt(scan.nextLine());
        int substituteCounter = 0;
        label:
        for (int i = n11; i <= 8; i++) {
            for (int j = 9; j >= n12; j--) {
                for (int k = n21; k <= 8; k++) {
                    for (int l = 9; l >= n22; l--) {
                        if ((i % 2 == 0 && j % 2 != 0) && (k % 2 == 0 && l % 2 != 0)) {
                            if (i == k && j == l) {
                                System.out.printf("Cannot change the same player.%n");
                            } else {
                                substituteCounter++;
                                if (substituteCounter <= 6) {
                                    System.out.printf("%d%d - %d%d%n", i, j, k, l);
                                } else {
                                    break label;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
