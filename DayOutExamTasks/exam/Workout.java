package DayOutExamTasks.exam;

import java.util.Scanner;

public class Workout {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int days = Integer.parseInt(scan.nextLine());
        double kilometers = Double.parseDouble(scan.nextLine());
        double totalKilometers =0;
        double currentKilometers = 0;
        for (int i = 0; i <=days ; i++) {
            if (i == 0){
                currentKilometers +=kilometers;
            }else{
                int increasedPercentPerDay = Integer.parseInt(scan.nextLine());
                currentKilometers += (currentKilometers*increasedPercentPerDay)/100;
            }
            totalKilometers += currentKilometers;
        }
        if (totalKilometers>=1000){
            System.out.printf("You've done a great job running %.0f more kilometers!", Math.ceil(totalKilometers-1000));
        }else{
            System.out.printf("Sorry Mrs. Ivanova, you need to run %.0f more kilometers", Math.ceil((1000-totalKilometers)));
        }
    }
}
