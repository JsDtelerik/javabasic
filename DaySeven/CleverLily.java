package DaySeven;

import java.util.Scanner;

public class CleverLily {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int age = Integer.parseInt(scan.nextLine());
        double washingMachinePrice = Double.parseDouble(scan.nextLine());
        int toyPrice = Integer.parseInt(scan.nextLine());
        double birthdayMP = 10.00;
        double moneyCollected = 0.0;
        double receivedMo = 0.0;
        int toyCount =0;
        for (int i = 1; i<=age; i++){
            if (i % 2 == 0){
                receivedMo += ((birthdayMP*i)/2);
            }else{
                toyCount ++;
            }
        }
         moneyCollected = toyCount*toyPrice+(receivedMo-(Math.floor(age/2)));
        if (moneyCollected>=washingMachinePrice){
            System.out.printf("Yes! %.02f", moneyCollected-washingMachinePrice);
        }else{
            System.out.printf("No! %.02f", washingMachinePrice-moneyCollected);
        }
    }
}
