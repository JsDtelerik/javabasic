package DaySeven;

import java.util.Scanner;

public class LeftAndRightSum {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = Integer.parseInt(scan.nextLine());
        int rightSum = 0;
        int leftSum = 0;
        for (int i = 0; i < 2*n; i++){
            int number = Integer.parseInt(scan.nextLine());
            if (i<n){
                leftSum+=number;
            }else{
                rightSum+=number;
            }
        }
        if (rightSum == leftSum){
            System.out.printf("Yes, sum = %d", leftSum);
        }else{
            System.out.printf("No, diff = %d", Math.abs(rightSum-leftSum));
        }
    }
}
