package DaySeven;

import java.util.Scanner;

public class NumberSequence {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = Integer.parseInt(scan.nextLine());
        int minNum = Integer.MAX_VALUE;
        int maxNum = Integer.MIN_VALUE;

        for (int i = 0; i < n; i++){
            int number = Integer.parseInt(scan.nextLine());
            if (number>maxNum){
                maxNum=number;
            }
            if(number<minNum){
            minNum=number;

            }

        }
        System.out.printf("Max number: %d%n" +
                "Min number: %d", maxNum, minNum);
    }

}
