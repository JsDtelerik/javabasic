package DaySeven;

import java.util.Scanner;

public class OddEvenSum {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = Integer.parseInt(scan.nextLine());
        int evenNumbers = 0;
        int oddNumbers = 0;
        for (int i = 1; i<=n; i++){
            int number = Integer.parseInt(scan.nextLine());
            if (i % 2 == 0){
                evenNumbers += number;
            }else{
                oddNumbers += number;
            }

        }
        if (evenNumbers == oddNumbers){
            System.out.printf("Yes%n" +
                    "Sum = %d", evenNumbers);
        }else{
            System.out.printf("No%n" +
                    "Diff = %d", Math.abs(evenNumbers-oddNumbers));
        }
    }

}
