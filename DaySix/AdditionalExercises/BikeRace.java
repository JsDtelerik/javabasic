package DaySix.AdditionalExercises;

import java.util.Scanner;

public class BikeRace {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int juniorBicyclistCount = Integer.parseInt(scan.nextLine());
        int seniorBicyclistCount = Integer.parseInt(scan.nextLine());
        String trace = scan.nextLine();
        double donation = 0.0;
        switch (trace.toLowerCase()){
            case "trail": donation=(((juniorBicyclistCount*5.5)+(seniorBicyclistCount*7))*0.95);  break;
            case "cross-country":
                if (juniorBicyclistCount+seniorBicyclistCount>=50){
                    donation=(((juniorBicyclistCount*(8.0*0.75))+(seniorBicyclistCount*(9.5*0.75)))*0.95);
                }else{
                    donation=(((juniorBicyclistCount*8.0)+(seniorBicyclistCount*9.5))*0.95);
                }
                break;
            case "downhill": donation=(((juniorBicyclistCount*12.25)+(seniorBicyclistCount*13.75))*0.95); break;
            case "road": donation=(((juniorBicyclistCount*20.00)+(seniorBicyclistCount*21.50))*0.95); break;
        }
        System.out.printf("%.2f", donation);

    }
}
