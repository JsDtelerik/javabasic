package DaySix.AdditionalExercises;

import javax.swing.*;
import java.util.Scanner;

public class CarToGo {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double budget = Double.parseDouble(scan.nextLine());
        String season = scan.nextLine();
        String classVehicle = "";
        String typeVehicle = "";
        double vehiclePrice = 0.0;
        if (budget <= 100) {
            classVehicle = "Economy class";
            switch (season.toLowerCase()) {
                case "summer":
                    typeVehicle = "Cabrio";
                    vehiclePrice = budget * 0.35;
                    break;
                case "winter":
                    typeVehicle = "Jeep";
                    vehiclePrice = budget * 0.65;
                    break;
            }
        } else if (budget <= 500) {
            classVehicle = "Compact class";
            switch (season.toLowerCase()) {
                case "summer":
                    typeVehicle = "Cabrio";
                    vehiclePrice = budget * 0.45;
                    break;
                case "winter":
                    typeVehicle = "Jeep";
                    vehiclePrice = budget * 0.80;
                    break;
            }
        } else {
            classVehicle= "Luxury class";
            typeVehicle = "Jeep";
            vehiclePrice =budget*0.90;
        }
        System.out.printf("%s%n%s - %.02f",classVehicle, typeVehicle,vehiclePrice);

    }

}