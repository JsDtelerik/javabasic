package DaySix.AdditionalExercises;

import java.util.Scanner;

public class Flowers {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int chrysanthemumsCount = Integer.parseInt(scan.nextLine());
        int rosesCount = Integer.parseInt(scan.nextLine());
        int tulipsCount = Integer.parseInt(scan.nextLine());
        String season = scan.nextLine();
        String holidayOrNot = scan.nextLine();
        double discount = 0.0;
        double bouquetPrice = 0.0;
        switch (season.toLowerCase()) {
            case "spring":
            case "summer":
                if (holidayOrNot.equalsIgnoreCase("y")) {
                    bouquetPrice = (chrysanthemumsCount * (2.00 * 1.15)) + (rosesCount * (4.10 * 1.15))
                            + (tulipsCount * (2.5 * 1.15));
                } else {
                    bouquetPrice = (chrysanthemumsCount * 2.00) + (rosesCount * 4.10)
                            + (tulipsCount * 2.5);
                }
                break;

            case "autumn":
            case "winter":
                if (holidayOrNot.equalsIgnoreCase("y")) {
                    bouquetPrice = (chrysanthemumsCount * (3.75 * 1.15)) + (rosesCount * (4.50 * 1.15))
                            + (tulipsCount * (4.15 * 1.15));
                } else {
                    bouquetPrice = (chrysanthemumsCount * 3.75) + (rosesCount * 4.50)
                            + (tulipsCount * 4.15);
                }
                break;
        }
        if (tulipsCount > 7 && season.equalsIgnoreCase("spring")) {
            discount = bouquetPrice * 0.95;
        }else if(rosesCount>=10 && season.equalsIgnoreCase("winter")){
            discount = bouquetPrice *0.90;
        }else{
            discount = bouquetPrice;
        }
        if (tulipsCount+chrysanthemumsCount+rosesCount>20){
            discount *= 0.80;
        }
        System.out.printf("%.02f",discount+2);

    }
}

