package DaySix.AdditionalExercises;

import java.util.Scanner;

public class MatchTickets {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double budget = Double.parseDouble(scan.nextLine());
        String category = scan.nextLine();
        int group = Integer.parseInt(scan.nextLine());
        double transportPrice = 0.0;
        double tickets = 0.0;
        double expenses = 0.0;
        String possibility = "";
        if (group>0 && group<5){
            transportPrice = budget*0.75;
        }else if (group>=5 && group<10){
            transportPrice = budget*0.60;
        }else if (group>=10 && group<25){
            transportPrice = budget*0.50;
        }else if (group>=25 && group<50){
            transportPrice = budget*0.40;
        }else
            transportPrice = budget*0.25;

        switch (category.toLowerCase()){
            case "vip":
                tickets = group*499.99;
                expenses=tickets+transportPrice;
                if (budget>=expenses){
                    possibility = String.format("Yes! You have %.02f leva left.", budget-expenses);
                }else{
                    possibility = String.format("Not enough money! You need %.02f leva.", expenses-budget);
                }
                break;
            case "normal":
                tickets = group*249.99;
                expenses= tickets+transportPrice;
                if (budget>=expenses){
                    possibility = String.format("Yes! You have %.02f leva left.", budget-expenses);
                }else{
                    possibility = String.format("Not enough money! You need %.02f leva.", expenses-budget);
                }
                break;
        }

        System.out.println(possibility);

    }
}
