package DaySix.AdditionalExercises;

import java.util.Scanner;

public class MultiplyByTwo {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = Integer.MAX_VALUE;
        while (n >= 0) {
            double number = Double.parseDouble(scan.nextLine());
            if (number >= 0) {
                System.out.printf("Result: %.2f%n", number * 2);

            }else{
                System.out.println("Negative number!");
                break;
            }


        }

    }
}



