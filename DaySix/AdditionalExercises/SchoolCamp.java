package DaySix.AdditionalExercises;

import java.util.Scanner;

public class SchoolCamp {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String season = scan.nextLine();
        String groupType = scan.nextLine();
        int students = Integer.parseInt(scan.nextLine());
        int overnights = Integer.parseInt(scan.nextLine());
        String sportActivities= "";
        double price = 0.0;
        double discount =0.0;
        switch (season.toLowerCase()){
            case "winter":
                if (groupType.equalsIgnoreCase("boys")) {
                    sportActivities = "Judo";
                    price = (students * 9.6) * overnights;
                }else if (groupType.equalsIgnoreCase("girls")){
                    price = (students * 9.6) * overnights;
                    sportActivities = "Gymnastics";
                }else{
                    sportActivities = "Ski";
                    price = (students*10.0)*overnights;
                }
                break;
            case "spring":
                if (groupType.equalsIgnoreCase("boys")) {
                    sportActivities = "Tennis";
                    price = (students * 7.2) * overnights;
                }else if (groupType.equalsIgnoreCase("girls")){
                    price = (students * 7.2) * overnights;
                    sportActivities = "Athletics";
                }else {
                    sportActivities = "Cycling";
                    price = (students * 9.5) * overnights;
                }
                break;
            case "summer":
                if (groupType.equalsIgnoreCase("boys")) {
                    sportActivities = "Football";
                    price = (students * 15.0) * overnights;
                }else if (groupType.equalsIgnoreCase("girls")){
                    price = (students * 15.0) * overnights;
                    sportActivities = "Volleyball";
                }else {
                    sportActivities = "Swimming";
                    price = (students * 20.0) * overnights;
                }
                break;

        }
        if (students>=10 && students<20){
            discount = price*0.95;
        }else if (students>=20 && students <50){
            discount = price*0.85;
        }else if (students>=50){
            discount = price *0.50;
        }else{
            discount=price;
        }

        System.out.printf("%s %.02f lv.", sportActivities, discount);

    }
}
