package DaySix.AdditionalExercises;

import java.util.Scanner;

public class TruckDriver {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String season = scan.nextLine();
        double kilometersPerMonth = Double.parseDouble(scan.nextLine());
        double salary = 0.0;
        switch (season.toLowerCase()){
            case "spring":
            case "autumn":
                if (kilometersPerMonth<=5000){
                    salary = kilometersPerMonth*0.75;
                }else if (kilometersPerMonth<=10000){
                    salary = kilometersPerMonth* 0.95;
                }else{
                    salary = kilometersPerMonth*1.45;
                }
                break;
            case "summer":
                if (kilometersPerMonth<=5000){
                    salary = kilometersPerMonth*0.90;
                }else if (kilometersPerMonth<=10000){
                    salary = kilometersPerMonth* 1.10;
                }else{
                    salary = kilometersPerMonth*1.45;
                }
                break;
            case "winter":
                if (kilometersPerMonth<=5000){
                    salary = kilometersPerMonth*1.05;
                }else if (kilometersPerMonth<=10000){
                    salary = kilometersPerMonth* 1.25;
                }else{
                    salary = kilometersPerMonth*1.45;
                }
                break;

        }

        System.out.printf("%.02f", ((salary*0.9)*4));

    }
}
