package DaySix.AdditionalExercises;


import java.util.Scanner;

public class Vacation {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double budget = Double.parseDouble(scan.nextLine());
        String season = scan.nextLine();
        String accommodation = "";
        String location = "";
        double price = 0.0;
        if (budget <= 1000) {
            accommodation = "Camp";
            switch (season.toLowerCase()) {
                case "summer": location = "Alaska"; price = budget * 0.65; break;
                case "winter": location = "Morocco"; price = budget * 0.45;break;
            }
        } else if (budget <= 3000) {
            accommodation = "Hut";
            switch (season.toLowerCase()) {
                case "summer":  location = "Alaska"; price = budget * 0.80;break;
                case "winter":  location = "Morocco"; price = budget * 0.60;break;
            }
        } else {
            accommodation = "Hotel";
            switch (season.toLowerCase()) {
                case "summer": location = "Alaska"; price = budget * 0.90; break;
                case "winter": location = "Morocco"; price = budget * 0.90; break;
            }
        }

        System.out.printf("%s - %s - %.02f", location, accommodation, price);

    }
}