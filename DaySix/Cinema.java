package DaySix;

import java.util.Scanner;

public class Cinema {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String projection = scan.nextLine();
        int lines = Integer.parseInt(scan.nextLine());
        int columns = Integer.parseInt(scan.nextLine());
        double result = 0.0;
        switch (projection){
            case "Premiere": result = (lines*columns)*12.00; break;
            case "Normal": result = (lines*columns)*7.50; break;
            case "Discount": result = (lines*columns)*5.00; break;
        }
        System.out.printf("%.2f leva", result);
    }
}
