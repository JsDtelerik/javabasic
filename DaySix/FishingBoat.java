package DaySix;

import java.util.Scanner;

public class FishingBoat {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int budget = Integer.parseInt(scan.nextLine());
        String season = scan.nextLine();
        int fishermen = Integer.parseInt(scan.nextLine());
        double rentDiscount = 0.0;
        switch (season){
            case "Spring":
                if(fishermen<=6){
                    rentDiscount = 3000*0.90;
                }else if (fishermen>6 && fishermen<=11){
                    rentDiscount = 3000*0.85;
                }else if (fishermen>=12){
                    rentDiscount = 3000*0.75;
                }
                break;
            case "Summer":
            case "Autumn":
                if(fishermen<=6){
                    rentDiscount = 4200*0.90;
                }else if (fishermen>6 && fishermen<=11){
                    rentDiscount = 4200*0.85;
                }else if (fishermen>=12){
                    rentDiscount = 4200*0.75;
                }
                break;
            case "Winter":
                if(fishermen<=6){
                    rentDiscount = 2600*0.90;
                }else if (fishermen>6 && fishermen<=11){
                    rentDiscount = 2600*0.85;
                }else if (fishermen>=12){
                    rentDiscount = 2600*0.75;
                }
                break;

        }

        if (fishermen%2 == 0 && !season.equalsIgnoreCase("Autumn")){
            rentDiscount *=0.95;
        }
        if (budget>=rentDiscount){

        System.out.printf("Yes! You have %.2f leva left.", budget-rentDiscount);

        }else{
            System.out.printf("Not enough money! You need %.2f leva.", rentDiscount-budget);
        }


    }
}
