package DaySix;

import java.util.Scanner;

public class HotelRoom {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String month = scan.nextLine();
        int overNights = Integer.parseInt(scan.nextLine());
        double priceForApartment = 0.0;
        double priceForStudio = 0.0;

        if (month.equalsIgnoreCase("May")|| month.equalsIgnoreCase("October")){
            priceForStudio=overNights*50;
            priceForApartment =overNights*65;
            if (overNights>14){
              priceForStudio *= 0.70;
              priceForApartment *= 0.90;
            }else if (overNights>7){
                priceForStudio *= 0.95;



            }

        }

        if(month.equalsIgnoreCase("June") || month.equalsIgnoreCase("September")){
            priceForApartment =68.70*overNights;
            priceForStudio =   75.2*overNights;
            if(overNights>14){
                priceForStudio *= 0.80;
                priceForApartment *=0.90;
            }
        }
        if (month.equalsIgnoreCase("July")|| month.equalsIgnoreCase("August")){
            priceForApartment =overNights*77.0;
            priceForStudio = overNights*76.0;
            if (overNights>14){
                priceForApartment *= 0.90;
            }
        }



        System.out.printf("Apartment: %.2f lv.%n" +
                "Studio: %.2f lv.", priceForApartment, priceForStudio);

    }
}
