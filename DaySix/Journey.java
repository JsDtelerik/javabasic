package DaySix;

import java.util.Scanner;

public class Journey {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double budget = Double.parseDouble(scan.nextLine());
        String season = scan.nextLine();
        double expenses = 0.0;
        String destination = "";
        String accommodation = "";
        if (budget<=100){
            destination = "Bulgaria";
            switch (season){
                case "summer": accommodation= "Camp"; expenses= budget* 0.3; break;
                case "winter": accommodation="Hotel"; expenses = budget*0.70; break;
            }
        }else if (budget<=1000){
            destination="Balkans";
            switch (season){
                case "summer": accommodation= "Camp"; expenses= budget* 0.40; break;
                case "winter": accommodation="Hotel"; expenses = budget*0.80; break;
            }
        }else if (budget>1000){
            destination="Europe"; accommodation= "Hotel";
            expenses=budget*0.90;
        }
        System.out.printf("Somewhere in %s%n" +
                "%s - %.2f", destination, accommodation, expenses);


    }

}
