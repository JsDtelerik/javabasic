package DaySix;

import java.util.Scanner;

public class NewHouse {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String flowers = scan.nextLine();
        int countFlowers = Integer.parseInt(scan.nextLine());
        int budget = Integer.parseInt(scan.nextLine());
        double flowersPrice = 0.0;

        switch (flowers){
            case "Roses":
                flowersPrice=5.00*countFlowers;
                if (countFlowers>80){
                    flowersPrice *=0.90;
                }
            break;
            case "Dahlias":
                flowersPrice=3.80*countFlowers;
                if (countFlowers>90){
                    flowersPrice *=0.85;
                }
                break;
            case "Tulips":
                flowersPrice=2.8*countFlowers;
                if (countFlowers>80){
                    flowersPrice *=0.85;
                }
                break;
            case "Narcissus":
                flowersPrice=3.00*countFlowers;
                if (countFlowers<120){
                    flowersPrice *=1.15;
                }
                break;
            case "Gladiolus":
                flowersPrice=2.50*countFlowers;
                if (countFlowers<80){
                    flowersPrice *= 1.20;
                }
                break;

        }


        if (budget>=flowersPrice){
            System.out.printf("Hey, you have a great garden with %d " +
                    "%s and %.2f leva left.", countFlowers, flowers, budget-flowersPrice);
        }else{
            System.out.printf("Not enough money, you need %.2f leva more.", flowersPrice-budget);
        }

            }


        }



