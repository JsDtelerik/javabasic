package DaySix;

import java.util.Scanner;

public class OnTimeForTheExam {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int examHour = Integer.parseInt(scan.nextLine());
        int examMinutes = Integer.parseInt(scan.nextLine());
        int arrivalHour = Integer.parseInt(scan.nextLine());
        int arrivalMinutes = Integer.parseInt(scan.nextLine());
        int examTime = (examHour * 60) + examMinutes;
        int arrivalTime = (arrivalHour * 60) + arrivalMinutes;
        String exam = "";
        int hours = Math.abs(examTime - arrivalTime) / 60;
        int minutes = Math.abs(examTime - arrivalTime) % 60;

        if (arrivalTime == examTime) {
            exam = "On time";
        } else if (arrivalTime < examTime) {
            if (hours == 0 && minutes <= 30) {
                exam = String.format("On time%n" +
                        "%d minutes before the start", examTime - arrivalTime);

            } else if (arrivalTime < examTime) {
                if (hours < 1) {
                    exam = String.format("Early%n" +
                            "%d minutes before the start\n", examTime - arrivalTime);
                } else if (hours > 0) {
                    exam = String.format("Early%n" +
                            "%d:%02d hours before the start", hours, minutes);
                }

            }

        } else if (examTime < arrivalTime) {

            if (hours < 1) {
                exam = String.format("Late%n" +
                        "%d minutes after the start\n", arrivalTime - examTime);
            } else if (hours >= 1) {
                exam = String.format("Late%n" +
                        "%d:%02d hours after the start", hours, minutes);
            }

        }

        System.out.println(exam);
    }


}
