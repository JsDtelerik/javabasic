package DaySix;

import java.util.Scanner;

public class OperationsBetweenNumbers {
    public static void main(String[] args) {
            Scanner scan = new Scanner(System.in);
            int n1 = Integer.parseInt(scan.nextLine());
            int n2 = Integer.parseInt(scan.nextLine());
            String operator = scan.nextLine();
            double result = 0.00;
            String oddOrEven = "";
            String finalResult = "";
                    switch (operator) {
                        case "+":
                            result = n1 + n2;
                            if (result % 2 == 0) {
                                oddOrEven = "even";
                            } else {
                                oddOrEven = "odd";
                            }
                            finalResult = String.format("%d %s %d = %.0f - %s", n1, operator, n2, result, oddOrEven);
                            break;

                        case "-":
                            result = n1 - n2;
                            if (result % 2 == 0) {
                                oddOrEven = "even";
                            } else {
                                oddOrEven = "odd";
                            }
                            finalResult = String.format("%d %s %d = %.0f - %s", n1, operator, n2, result, oddOrEven);
                            break;
                        case "*":
                            result = n1 * n2;
                            if (result % 2 == 0) {
                                oddOrEven = "even";
                            } else {
                                oddOrEven = "odd";
                            }
                            finalResult = String.format("%d %s %d = %.0f - %s", n1, operator, n2, result, oddOrEven);
                            break;

                        case "%":
                            if (n1==0){
                                finalResult = String.format("Cannot divide %d by zero", n2);
                            }else if (n2 == 0) {
                                finalResult = String.format("Cannot divide %d by zero", n1);
                             }else if (n1%n2>0){
                                     result =n1%n2;
                                finalResult = String.format("%d %s %d = %.0f", n1, operator, n2, result);

                    }break;

                        case "/":
                            if (n1 == 0) {
                                finalResult = String.format("Cannot divide %d by zero", n2);
                            }else if (n2 == 0) {
                                finalResult = String.format("Cannot divide %d by zero", n1);
                            }else if (n1 > 0 && n2 > 0){
                                result = ((float) n1) /n2;
                                finalResult = String.format("%d %s %d = %.2f", n1, operator, n2, result);

                            }break;

            }


        System.out.println(finalResult);
    }




}
