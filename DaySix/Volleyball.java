package DaySix;

import java.util.Scanner;

public class Volleyball {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String leapOrNormal = scan.nextLine();
        int holidays = Integer.parseInt(scan.nextLine());
        int weekendsTrips = Integer.parseInt(scan.nextLine());
        double playingDays = 0.0;
        switch (leapOrNormal.toLowerCase()){
            case "leap":
                playingDays = ((48-weekendsTrips)*0.75)+(holidays*0.6666666666666667)+weekendsTrips;
                playingDays *=1.15; break;
            case "normal":
                playingDays = ((48-weekendsTrips)*0.75)+(holidays*0.6666666666666667)+weekendsTrips;
                break;
        }
        System.out.printf("%.0f", Math.floor(playingDays));
    }
}
