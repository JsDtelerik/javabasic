package DayTen.AdditionalExercises;

import java.util.Scanner;

public class Dishwasher {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int bottlesDetergent = Integer.parseInt(scan.nextLine());
        String input = scan.nextLine();
        int detergentMl = bottlesDetergent*750;
        int cycles = 0;
        int pots = 0;
        int plates = 0;
        boolean isOver = false;
        while (!input.equals("End")){
            int dishes = Integer.parseInt(input);
            cycles++;
            if (cycles %3 == 0){
                detergentMl -= dishes*15;
                pots += dishes;
            }else{
                detergentMl -= dishes*5;
                plates+= dishes;
            }
            if (detergentMl<0){
                isOver = true;
                break;
            }
            input = scan.nextLine();
        }
        String output = "";
        if (isOver){
            output = String.format("Not enough detergent, %d ml. more necessary!", Math.abs(detergentMl));
        }else{
            output = String.format("Detergent was enough!%n" +
                    "%d dishes and %d pots were washed.%n" +
                    "Leftover detergent %d ml.", plates, pots, detergentMl);
        }
        System.out.println(output);
    }
}
