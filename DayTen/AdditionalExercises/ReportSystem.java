package DayTen.AdditionalExercises;

import java.util.Scanner;

public class ReportSystem {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int requiredMoney = Integer.parseInt(scan.nextLine());
        String input = scan.nextLine();
        int paidWithCard = 0;
        int paidInCash = 0;
        int rounds = 0;
        int donorsCards = 0;
        int donorsCash = 0;
        boolean success = false;
        while (!input.equals("End")){
            int donatedMoney = Integer.parseInt(input);
            rounds++;
            if (rounds %2 == 0){
                if (donatedMoney>=10){
                    paidWithCard +=donatedMoney;
                    donorsCards++;
                    System.out.println("Product sold!");
                }else{
                    System.out.println("Error in transaction!");
                }
            }else{
                if (donatedMoney<=100){
                    paidInCash +=donatedMoney;
                    donorsCash++;
                    System.out.println("Product sold!");
                }else{
                System.out.println("Error in transaction!");
                }

            }
            int total = paidInCash+paidWithCard;
            if (requiredMoney<=total){
                success = true;
                break;
            }
            input = scan.nextLine();

        }
        if (success){
            System.out.printf("Average CS: %.02f%n" +
                    "Average CC: %.02f",(float)paidInCash/donorsCash, (float)paidWithCard/donorsCards);
        }else {
            System.out.println("Failed to collect required money for charity.");
        }

    }
}
