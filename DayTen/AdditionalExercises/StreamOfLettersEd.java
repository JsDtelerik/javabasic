package DayTen.AdditionalExercises;

import java.util.Scanner;

public class StreamOfLettersEd {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        boolean cAppeared = false;
        boolean oAppeared = false;
        boolean nAppeared = false;
        String word = "";
        String input = scan.nextLine();
        while (!input.equals("End")){
            char letter = input.charAt(0);
            boolean addLetter = false;
            if ((letter>= 'a' && letter<= 'z') || (letter>= 'A' && letter<= 'Z')) {
                switch (letter) {
                    case 'c': addLetter = cAppeared; cAppeared = true; break;
                    case 'o': addLetter = oAppeared; oAppeared = true; break;
                    case 'n': addLetter = nAppeared; nAppeared = true; break;
                    default:
                        addLetter = true;
                        break;
                }
            }
                if (addLetter) {
                    word += letter;
                }
                if (cAppeared && oAppeared && nAppeared) {
                    System.out.print(word + ' ');
                    word = "";
                    cAppeared = false;
                    oAppeared = false;
                    nAppeared = false;
                }
            input = scan.nextLine();
            }

        }

    }

