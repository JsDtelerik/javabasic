package DayTen;

import java.util.Scanner;

public class Cake {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int cakeLength = Integer.parseInt(scan.nextLine());
        int cakeWight = Integer.parseInt(scan.nextLine());
        String input = scan.nextLine();
        int totalCakePieces = cakeLength*cakeWight;
        int eatenPieces = 0;
        boolean cakeIsOver = false;
        while (!input.equals("STOP")){
            int pieces = Integer.parseInt(input);
            eatenPieces +=pieces;
            if (totalCakePieces<=eatenPieces){
                cakeIsOver = true;
                break;
            }
            input = scan.nextLine();
        }
        String result = "";
        if (cakeIsOver){
            result = String.format("No more cake left! You need %d pieces more.", eatenPieces-totalCakePieces);
        }else {
            result = String.format("%d pieces are left.", totalCakePieces-eatenPieces);
        }
        System.out.println(result);
    }
}
