package DayTen;

import java.util.Scanner;

public class Coins {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double change = Double.parseDouble(scan.nextLine());

        double changeCents = Math.floor(change*100);
        int coinCounter = 0;
        boolean noMoreChange = false;
        while (!noMoreChange){
            if (changeCents>=200){
                changeCents-=200;
                coinCounter++;
            }else if (changeCents>=100){
                changeCents -=100;
                coinCounter++;
            }else if (changeCents>=50) {
                changeCents -= 50;
                coinCounter++;
            }else if (changeCents>=20) {
                changeCents -= 20;
                coinCounter++;
            } else if (changeCents>=10) {
                changeCents -= 10;
                coinCounter++;
            }else if (changeCents>=5) {
                changeCents -= 5;
                coinCounter++;
            } else if (changeCents>=2) {
                changeCents -= 2;
                coinCounter++;
            }else if (changeCents>=1) {
                changeCents -= 1;
                coinCounter++;

            }
            if (changeCents == 0){
                noMoreChange = true;
                break;
            }

        }
        if (noMoreChange){
            System.out.println(coinCounter);
        }
    }
}
