package DayTen;

import java.util.Scanner;

public class ExamPreparation {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int poorMarksAllowed = Integer.parseInt(scan.nextLine());
        String input = scan.nextLine();
        double allGrades = 0.0;
        int problemCount = 0;
        String lastProblem = "";
        int fails =0;
        boolean poorMarksAllowedIsReached = false;
        while (!input.equals("Enough")){
            int grade = Integer.parseInt(scan.nextLine());
            allGrades +=grade;
            problemCount++;
            if (grade<=4.00){
                fails++;
                if (poorMarksAllowed==fails){
                    poorMarksAllowedIsReached = true;
                    break;
                }
            }
            lastProblem = input;
            input = scan.nextLine();
        }
        double average = allGrades/problemCount;
        String output = "";
        if (poorMarksAllowedIsReached){
            output = String.format("You need a break, %d poor grades.", fails);
        }else{
            output = String.format("Average score: %.02f%n" +
                    "Number of problems: %d%n" +
                    "Last problem: %s", average, problemCount, lastProblem);
        }
        System.out.println(output);
    }
}
