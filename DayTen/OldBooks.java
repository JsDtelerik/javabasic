package DayTen;

import java.util.Scanner;

public class OldBooks {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        String book = scan.nextLine();
        String input = scan.nextLine();
        int bookCounter = 0;
        boolean isFound = false;
        while (!input.equals("No More Books")){
            if (input.equals(book)){
                isFound = true;
                break;
            }

            bookCounter++;
            input = scan.nextLine();
        }
        String output = "";
        if (isFound){
            output = String.format("You checked %d books and found it.", bookCounter);
        }else {
            output = String.format("The book you search is not here!%n" +
                    "You checked %d books.", bookCounter);
        }
        System.out.println(output);
    }
}
