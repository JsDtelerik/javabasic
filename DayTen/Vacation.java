package DayTen;

import java.util.Scanner;

public class Vacation {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double tripCost = Double.parseDouble(scan.nextLine());
        double ownedMoney = Double.parseDouble(scan.nextLine());
        String action = scan.nextLine();
        int spendCounter = 0;
        boolean isFailed = false;
        int days = 0;
        while (tripCost>ownedMoney){
            double sum = Double.parseDouble(scan.nextLine());
            days++;
            if (action.equals("spend")){
                spendCounter++;
                ownedMoney -=sum;
                if (spendCounter==5){
                    isFailed = true;
                    break;
                }else if (ownedMoney<=0){
                    ownedMoney=0;
                }

            }else if (action.equals("save")){
                spendCounter=0;
                ownedMoney +=sum;
            }
        if (ownedMoney>=tripCost){
            continue;
        }
            action = scan.nextLine();
        }
        if (isFailed){
            System.out.printf("You can't save the money.%n" +
                    "%d", days);

        }else {
            System.out.printf("You saved the money for %d days.", days);
        }
    }
}
