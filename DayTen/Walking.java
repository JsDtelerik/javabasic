package DayTen;

import java.util.Scanner;

public class Walking {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String input = scan.nextLine();

        int goal = 10000;
        int steps = 0;
        while (goal>=steps){
            if (input.equals("Going home")) {
                int newSteps = Integer.parseInt(scan.nextLine());
                steps += newSteps;
                break;
            }
            int stepsWalked = Integer.parseInt(input);
            steps +=stepsWalked;
            if (steps>=goal){
                continue;
            }
            input = scan.nextLine();

            }

        String result = "";
        if (steps<goal){
            result = String.format("%d more steps to reach goal.", goal -steps);
        }else {
            result = String.format("Goal reached! Good job!%n" +
                    "%d steps over the goal!",  steps-goal);
        }
        System.out.println(result);
    }
}
