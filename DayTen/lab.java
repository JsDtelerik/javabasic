package DayTen;

import java.util.Scanner;

public class lab {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        int widthCake = Integer.parseInt(scan.nextLine());
        int lengthCake = Integer.parseInt(scan.nextLine());

        String action = scan.nextLine(); // въвеждаме командата или брой парчета торта или СТОП

        int allCakePieces = widthCake * lengthCake;
        boolean isFinished = false;

        while (!action.equals("STOP")) { //докато въведеното е различно от СТОП (вкарваме брой парчета или СТОП)

            int currentCakePieces = Integer.parseInt(action);  //въвеждаме колко парчета са извадени
            allCakePieces -= currentCakePieces; //изваждаме броя извадени парчета от общия брой

            if (allCakePieces <= 0) { //проверка дали тортата е свършила
                isFinished = true;
                break;
            }

            action = scan.nextLine(); // въвеждаме следващоата команда от конзолата - брой парче или СТОП

        }
        String output = "";
        if (isFinished) {
            output = String.format("No more cake left! You need %d pieces more.",Math.abs(allCakePieces));
        } else
            output = String.format("%d pieces are left.", allCakePieces);
        System.out.println(output);
    }

}






