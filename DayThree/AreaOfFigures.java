package DayThree;

import java.util.Scanner;

public class AreaOfFigures {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String shape = scan.nextLine();
        double area = 0.0;

        if (shape.equals("square")) {
            double a = Double.parseDouble(scan.nextLine());
            area = a * a;
        }else if (shape.equals("rectangle")){
            double sideA = Double.parseDouble(scan.nextLine());
            double sideB = Double.parseDouble(scan.nextLine());
            area = sideA*sideB;
        }else if (shape.equals("circle")){
            double radius = Double.parseDouble(scan.nextLine());
            area = Math.PI*(radius*radius);
        }else if (shape.equals("triangle")){
            double sideTr = Double.parseDouble(scan.nextLine());
            double sideTr2 = Double.parseDouble(scan.nextLine());
            area = (sideTr*sideTr2)/2;}
        System.out.printf("%.3f", area);


    }
}
