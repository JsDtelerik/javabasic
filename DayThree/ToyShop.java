package DayThree;

import java.util.Scanner;

public class ToyShop {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double discount = 0.0;
        double tripPrice = Double.parseDouble(scan.nextLine());
        int puzzlesCount = Integer.parseInt(scan.nextLine());
        int dollsCount = Integer.parseInt(scan.nextLine());
        int bearsCount = Integer.parseInt(scan.nextLine());
        int minionsCount = Integer.parseInt(scan.nextLine());
        int trucksCount = Integer.parseInt(scan.nextLine());
        int soldToys = puzzlesCount+dollsCount+bearsCount+minionsCount+trucksCount;
        double soldToysProfit = ((puzzlesCount*2.60)+(dollsCount*3.00)+(bearsCount*4.10)+(minionsCount*8.2)+(trucksCount*2.00));
        if (soldToys>=50){
             discount = soldToysProfit*0.25;}
        double profit = soldToysProfit-discount;
        double rent = profit*0.10;
        double realProfit= profit-rent;
        if (realProfit>=tripPrice)
            System.out.printf("Yes! %.2f lv left.", realProfit-tripPrice);
        else
            System.out.printf("Not enough money! %.2f lv needed.", tripPrice - realProfit);


    }
}
