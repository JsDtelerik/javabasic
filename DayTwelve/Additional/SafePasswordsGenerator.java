package DayTwelve.Additional;

import java.util.Scanner;

public class SafePasswordsGenerator {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int aNum = Integer.parseInt(scan.nextLine());
        int bNum = Integer.parseInt(scan.nextLine());
        int maxCombinations = Integer.parseInt(scan.nextLine());
        int combinationCounter = 0;
        label:
        for (int i = 35; i < 35+aNum; i++) {
            for (int j = 64; j < 64+bNum; j++) {
                for (int k = 1; k <= aNum; k++) {
                    for (int l = 1; l <= bNum; l++) {
                        combinationCounter++;
                        System.out.printf("%c%c%d%d%c%c|", i, j, k, l, j, i);
                        if (combinationCounter >= 1) {
                            i++;
                            j++;
                            if (i > 55) {
                                i = 35;
                            }
                            if (j > 96) {
                                j = 64;
                            }
                            if (combinationCounter == maxCombinations) {
                                break label;
                            }
                        }
                    }
                }
            }
        }
    }
}




