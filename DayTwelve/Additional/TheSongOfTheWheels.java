package DayTwelve.Additional;

import java.util.Scanner;

public class TheSongOfTheWheels {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int m = Integer.parseInt(scan.nextLine());
        int combinationCounter = 0;
        int keepI = 0;
        int keepJ = 0;
        int keepK = 0;
        int keepL = 0;
        boolean weHaveIt = false;
        for (int i = 1; i <= 9; i++) {
            for (int j = 1; j <= 9; j++) {
                for (int k = 1; k <= 9; k++) {
                    for (int l = 1; l <= 9; l++) {
                        if (i < j && k > l) {
                            if (((i * j) + (k * l) == m)) {
                                combinationCounter++;
                                System.out.printf("%d%d%d%d ", i, j, k, l);
                                if (combinationCounter == 4) {
                                    keepI = i; keepJ = j; keepK = k; keepL = l;
                                    weHaveIt = true;
                                }
                            }
                        }
                    }
                }
            }
        }
        if (weHaveIt) {
            System.out.printf("%nPassword: %d%d%d%d", keepI, keepJ, keepK, keepL);
        } else {
            System.out.printf("%nNo!");
        }
    }
}



