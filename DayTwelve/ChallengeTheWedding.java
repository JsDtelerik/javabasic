package DayTwelve;
import java.util.Scanner;

public class ChallengeTheWedding {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int m = Integer.parseInt(scan.nextLine());
        int f = Integer.parseInt(scan.nextLine());
        int maxTables = Integer.parseInt(scan.nextLine());
        label:
        for (int i = 1; i <=m ; i++) {
            for (int j = 1; j <=f ; j++) {
                System.out.printf("(%d <-> %d) ", i, j);
                maxTables--;
                if (maxTables == 0){
                    break label;
                }
            }
        }
    }
}
