package DayTwelve;

import java.util.Scanner;

public class HappyCatParking {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int days = Integer.parseInt(scan.nextLine());
        int hours = Integer.parseInt(scan.nextLine());
        double paidFeeForParking = 0;
        double sumPerDay = 0;
        for (int i = 1; i <=days ; i++) {
            sumPerDay =0;
            for (int j = 1; j <=hours ; j++) {
                if (i % 2 == 0){
                    if (j % 2== 0){
                        sumPerDay += 1;
                    }else{
                        sumPerDay +=2.5;
                    }
                }else if (i % 2 !=0){
                    if (j % 2 !=0){
                    sumPerDay +=1;
                    }else {
                        sumPerDay +=1.25;
                    }
                }
            }
            System.out.printf("Day: %d - %.02f leva%n", i, sumPerDay);
            paidFeeForParking += sumPerDay;
        }
        System.out.printf("Total: %.02f leva", paidFeeForParking);
    }
}
