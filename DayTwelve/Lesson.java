package DayTwelve;

import java.util.Scanner;

public class Lesson {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        int guests = Integer.parseInt(scan.nextLine());
        double budget = Double.parseDouble(scan.nextLine());

        double cakeNum = 0.0;
        if(guests%3 == 0) {
            cakeNum = Math.ceil(1.0*guests/3);
        }else {
            cakeNum = Math.ceil(1.0*guests/3);
        }

        int eggsNum = guests * 2;
        double cakePrice = 4 * cakeNum;
        double eggPrice = 0.45 * eggsNum;
        double sum = Math.floor(cakePrice) + eggPrice;

        if (sum <= budget) {
            System.out.printf("Lyubo bought %.0f Easter bread and %d eggs.%nHe has %.02f lv. left.", Math.floor(cakeNum), eggsNum,
                    budget - sum);
        } else {
            System.out.printf("Lyubo doesn't have enough money.%nHe needs %.02f lv. more.", sum - budget);
        }

    }

}



