package DayTwelve;

import java.util.Scanner;

public class LettersCombinations {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String str1 = scan.nextLine();
        String str2 = scan.nextLine();
        String str3 = scan.nextLine();
        char a = str1.charAt(0);
        char b = str2.charAt(0);
        char c = str3.charAt(0);
        int combinationCount = 0;
        for (int i = a; i <= b; i++) {
            for (int j = a; j <= b; j++) {
                for (int k = a; k <= b; k++) {
                    if (i != c && j != c && k != c) {
                        combinationCount++;
                        System.out.printf("%c%c%c ", i, j, k);
                    }
                }
            }
        }
        System.out.print(combinationCount);
    }
}
