package DayTwelve;

import java.util.Scanner;

public class PrimePairs {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int startFirstPair = Integer.parseInt(scan.nextLine());
        int startSecondPair = Integer.parseInt(scan.nextLine());
        int differenceInPairOne = Integer.parseInt(scan.nextLine());
        int differenceInPairTwo = Integer.parseInt(scan.nextLine());
        int keepI = 0;
        int keepJ = 0;
        for (int i = startFirstPair; i <= startFirstPair + differenceInPairOne; i++) {
            for (int j = startSecondPair; j <= startSecondPair + differenceInPairTwo; j++) {
                for (int k = 1; k <= i; k++) {
                    for (int l = 2; l <= j; l++) {
                        if (i % k == 0 && j % l == 0) {
                            if (i % 2 != 0 && j % 2 != 0) {
                                if (i % 3 != 0 && j % 3 != 0) {
                                    if (i % 5 != 0 && j % 5 != 0) {
                                        if (i % 7 != 0 && j % 7 != 0) {
                                            if (keepI != i || keepJ != j) {
                                                keepI = i;
                                                keepJ = j;
                                                System.out.printf("%d%d%n", i, j);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}