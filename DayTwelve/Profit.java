package DayTwelve;

import java.util.Scanner;

public class Profit {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int oneLevCoins = Integer.parseInt(scan.nextLine());
        int twoLevsCoins = Integer.parseInt(scan.nextLine());
        int fiveLevsBills = Integer.parseInt(scan.nextLine());
        int withdrawnAmount = Integer.parseInt(scan.nextLine());
        for (int i = 0; i <= oneLevCoins; i++) {
            for (int j = 0; j <= twoLevsCoins; j++) {
                for (int k = 0; k <= fiveLevsBills; k++) {
                    if (((i * 1) + (j * 2) + (k * 5)) == withdrawnAmount) {
                        System.out.printf("%d * 1 lv. + %d * 2 lv. + " +
                                "%d * 5 lv. = %d lv.%n", i, j, k, withdrawnAmount);
                    }
                }
            }
        }
    }
}
