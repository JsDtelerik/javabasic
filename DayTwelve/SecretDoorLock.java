package DayTwelve;

import java.util.Scanner;

public class SecretDoorLock {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int maxNum1 = Integer.parseInt(scan.nextLine());
        int maxNum2 = Integer.parseInt(scan.nextLine());
        int maxNum3 = Integer.parseInt(scan.nextLine());
label:
        for (int i = 1; i <= maxNum1; i++) {
            for (int j = 1; j <= maxNum2; j++) {
                for (int k = 1; k <= maxNum3; k++) {
                    if (maxNum1>9 || maxNum2>9 || maxNum3>9){
                        break label;
                    }
                    if (i % 2== 0 && k % 2 == 0){
                        if (j == 2 || j == 3 || j == 5 || j == 7){
                            System.out.printf("%d %d %d%n", i, j, k);
                        }
                    }

                }

            }

        }
    }
}
