package DayTwelve;

import java.util.Scanner;

public class SumOfTwoNumbers {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int start = Integer.parseInt(scan.nextLine());
        int end = Integer.parseInt(scan.nextLine());
        int magicNum = Integer.parseInt(scan.nextLine());
        int allCombinationsCounter = 0;
        int keepI = 0;
        int keepJ = 0;
        boolean magicCombinationFound = false;
        label:
        for (int i = start; i <=end; i++) {
            for (int j = start; j <=end ; j++) {
                allCombinationsCounter++;
                if (i+j == magicNum){
                    magicCombinationFound = true;
                    keepI = i;
                    keepJ = j;
                    break label;
                }
            }
        }
        if (magicCombinationFound){
            System.out.printf("Combination N:%d (%d + %d = %d)", allCombinationsCounter, keepI, keepJ, magicNum);
        }else{
            System.out.printf("%d combinations - neither equals %d", allCombinationsCounter, magicNum);
        }
    }
}
