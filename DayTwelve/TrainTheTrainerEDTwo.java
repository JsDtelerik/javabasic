package DayTwelve;

import java.util.Scanner;

public class TrainTheTrainerEDTwo {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int jury = Integer.parseInt(scan.nextLine());
        String presentation = scan.nextLine();
        double totalScore = 0.0;
        int presentationCounter = 0;
        while (!presentation.equals("Finish")){
            String currentPresentation = presentation;
            double gradeForPresentation = 0.0;
            presentationCounter++;
            for (int i = 1; i <= jury ; i++) {
                double score = Double.parseDouble(scan.nextLine());
                gradeForPresentation += score;
                totalScore += score;
                    if (i==jury){
                        double avgScorePerPresentation = gradeForPresentation/jury;
                        System.out.printf("%s - %.02f.%n", currentPresentation, avgScorePerPresentation);
                        presentation = scan.nextLine();
                        break;
                    }
            }
        }
        double finalAssessmentScore = totalScore/(presentationCounter*jury);
        System.out.printf("Student's final assessment is %.02f.", finalAssessmentScore);
    }
}
