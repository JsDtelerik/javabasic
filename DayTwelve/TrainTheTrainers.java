package DayTwelve;

import java.util.Scanner;

public class TrainTheTrainers {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int jury = Integer.parseInt(scan.nextLine());
        String presentation = scan.nextLine();
        int presentationCounter =0;
        double allGrades = 0.0;
        for (int i = 1; i <=jury ; i++) {
            String currentPresentation = "";
            double gradeForPresentation =0.0;

             int scoresCounter = 0;
            while (!presentation.equals("Finish")){
                double score = Double.parseDouble(scan.nextLine());
                currentPresentation = presentation;
                gradeForPresentation+=score;
                scoresCounter++;
                if (scoresCounter == jury){
                    double avgGradeForPresentation = gradeForPresentation/jury;
                    System.out.printf("%s - %.02f.%n", currentPresentation, avgGradeForPresentation);
                    presentationCounter++;
                    allGrades += gradeForPresentation;
                    presentation = scan.nextLine();
                    scoresCounter = 0;
                    gradeForPresentation = 0;
                }

            }
                if (jury == i){
                    double finalAssement = allGrades/(presentationCounter*jury);
                    System.out.printf("Student's final assessment is %.02f.", finalAssement);
                }

        }

    }
}
