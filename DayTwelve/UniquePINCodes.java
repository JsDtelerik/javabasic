package DayTwelve;

import java.util.Scanner;

public class UniquePINCodes {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int maxN1 = Integer.parseInt(scan.nextLine());
        int maxN2 = Integer.parseInt(scan.nextLine());
        int maxN3 = Integer.parseInt(scan.nextLine());
        for (int i = 1; i <= maxN1; i++) {
            for (int j = 1; j <= maxN2 ; j++) {
                for (int k = 1; k <=maxN3 ; k++) {
                    if ( i % 2 == 0){
                        if ( j == 2 || j == 3 || j == 5 || j==7 ){
                            if (k % 2 == 0){
                                System.out.println(i+" " + j + " " + k);
                            }
                        }
                    }
                }
            }
        }
    }
}
