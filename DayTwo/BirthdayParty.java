package DayTwo;

import java.util.Scanner;

public class BirthdayParty {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
   //⦁	Торта  – цената ѝ е 20% от наема на залата
        //⦁	Напитки – цената им е 45% по-малко от тази на тортата
        //⦁	Аниматор – цената му е 1/3 от цената за наема на залата
        int rentHall = Integer.parseInt(scan.nextLine());
        double cake = rentHall*0.2;
        double drinks = cake * 0.55;
        double animator = rentHall/3;
        double total = rentHall+cake+drinks+animator;
        System.out.println(total);

    }
}
