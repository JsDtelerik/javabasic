package DayTwo;

import java.util.Scanner;

public class CharityCampaigh {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int cakePrice = 45;
        double wafflePrice = 5.80;
        double pancakePrice = 3.20;
        int campaignDays = Integer.parseInt(scan.nextLine());
        int confectioners = Integer.parseInt(scan.nextLine());
        int cakesNum = Integer.parseInt(scan.nextLine());
        int wafflesNum = Integer.parseInt(scan.nextLine());
        int pancakesNum = Integer.parseInt(scan.nextLine());
        int cakesSumPerDay = cakesNum*cakePrice;
        double wafflesSumPerDay = wafflesNum*wafflePrice;
        double pancakesSumPerDay = pancakesNum*pancakePrice;
        double confectionersSum = (cakesSumPerDay+wafflesSumPerDay+pancakesSumPerDay)*confectioners;
        double sumCollected = confectionersSum*campaignDays;
        double sumForCharity = sumCollected - (sumCollected*0.125);
        System.out.printf("%.2f", sumForCharity);


    }
}
