package DayTwo;

import java.util.Scanner;

public class DepositCalc {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Deposit sum");
        double depositSum = Double.parseDouble(scan.nextLine());
        System.out.println("Deposit period");
        int depositPeriod = Integer.parseInt(scan.nextLine());
        System.out.println("Interest");
        double interest = Double.parseDouble(scan.nextLine());
        double interestPerMonth = (((depositSum*interest)/100)/12);
        double sum = depositSum + (depositPeriod*interestPerMonth);
        System.out.printf("%.2f", sum);

    }

}
