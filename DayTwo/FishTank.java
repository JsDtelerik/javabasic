package DayTwo;

import java.util.Scanner;

public class FishTank {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int leighCm = Integer.parseInt(scan.nextLine());
        int weighCm = Integer.parseInt(scan.nextLine());
        int heightCm = Integer.parseInt(scan.nextLine());
        double percent = Double.parseDouble(scan.nextLine());
        double volume = (leighCm*weighCm*heightCm)*0.001;
        double litres = volume - ((volume*percent)/100);
        System.out.printf("%.2f", litres);

    }

}

