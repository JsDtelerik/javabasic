package DayTwo;

import java.util.Scanner;

public class FruitMarket {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double strawberriesPrice = Double.parseDouble(scan.nextLine());
        double bananasKi = Double.parseDouble(scan.nextLine());
        double orangesKi = Double.parseDouble(scan.nextLine());
        double raspberriesKi = Double.parseDouble(scan.nextLine());
        double strawberriesKi = Double.parseDouble(scan.nextLine());
        double raspberriesPrice = strawberriesPrice*0.5;
        double orangesPrice = raspberriesPrice*0.6;
        double bananasPrice = raspberriesPrice*0.2;
        double sumStrawberries = strawberriesKi*strawberriesPrice;
        double sumBananas = bananasKi*bananasPrice;
        double sumOranges = orangesKi*orangesPrice;
        double sumRaspberries = raspberriesKi*raspberriesPrice;
        double sumFruits = sumStrawberries+sumBananas+sumOranges+sumRaspberries;
        System.out.printf("%.2f", sumFruits);


    }
}
