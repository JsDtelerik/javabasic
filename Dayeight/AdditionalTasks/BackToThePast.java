package Dayeight.AdditionalTasks;

import java.util.Scanner;

public class BackToThePast {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double inheritanceMoney = Double.parseDouble(scan.nextLine());
        int prognosis = Integer.parseInt(scan.nextLine());
            int age = 17;
         for (int year = 1800; year<=prognosis; year++){
             age++;
            if (year % 2 == 0){
                inheritanceMoney -=12000;
            }else{
                inheritanceMoney -=(12000+(age*50));
            }
        }
        if (inheritanceMoney>=0){
            System.out.printf("Yes! He will live a carefree life and will have %.02f dollars left.", inheritanceMoney);
        }else{
            System.out.printf("He will need %.02f dollars to survive.", Math.abs(inheritanceMoney));
        }
    }
}
