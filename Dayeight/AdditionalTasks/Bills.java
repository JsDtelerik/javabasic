package Dayeight.AdditionalTasks;

import java.util.Scanner;

public class Bills {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int month = Integer.parseInt(scan.nextLine());
        double electricityBill = 0.0;
        double waterBill = 0.0;
        double internetBill =0.0;
        double otherBill = 0.0;
        for (int i = 1; i<=month; i++){
            double electricity = Double.parseDouble(scan.nextLine());
                electricityBill += electricity;
                waterBill += 20;
                internetBill += 15;
                otherBill += ((electricity+20+15)*1.2);

        }
            double average = (electricityBill+waterBill+internetBill+otherBill)/month;
        System.out.printf("Electricity: %.02f lv%n" +
                "Water: %.02f lv%n" +
                "Internet: %.02f lv%n" +
                "Other: %.02f lv%n" +
                "Average: %.02f lv", electricityBill, waterBill, internetBill, otherBill, average);
    }
}
