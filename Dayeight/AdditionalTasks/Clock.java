package Dayeight.AdditionalTasks;

import java.text.DecimalFormat;

public class Clock {
    public static void main(String[] args) {
        DecimalFormat formatter = new DecimalFormat(" 0 ");
        for (int hours = 0; hours<=23; hours++){
            for (int minutes = 0; minutes<=59; minutes++){
                System.out.println(formatter.format(hours)+":"+
                        formatter.format(minutes));
            }
        }

    }
}
