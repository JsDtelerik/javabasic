package Dayeight.AdditionalTasks;

import java.lang.invoke.StringConcatFactory;
import java.text.DecimalFormat;

public class ClockPartTwo {
    public static void main(String[] args) {
        DecimalFormat formatter= new DecimalFormat(" 0 ");


        for (int hours = 0; hours <= 23; hours++)
        {
            for (int minutes = 0; minutes <= 59; minutes++)
            {
                for (int seconds = 0; seconds <= 59; seconds++)
                {
                    System.out.println(formatter.format(hours) + ":" +
                            formatter.format(minutes) + ":" +
                            formatter.format(seconds));
                }
            }
        }
    }
}


