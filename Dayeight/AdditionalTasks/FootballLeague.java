package Dayeight.AdditionalTasks;

import java.util.Scanner;

public class FootballLeague {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int stadiumCapacity = Integer.parseInt(scan.nextLine());
        int fans = Integer.parseInt(scan.nextLine());
        int sectorACounter =0;
        int sectorBCounter =0;
        int sectorVCounter =0;
        int sectorGCounter =0;
        for (int i = 1; i<= fans; i++){
            String sector = scan.nextLine();
            switch (sector){
                case "A": sectorACounter++; break;
                case "B": sectorBCounter++; break;
                case "V": sectorVCounter++; break;
                case "G": sectorGCounter++; break;
            }

        }
        double allFansPercent = 1.0*fans/stadiumCapacity*100;
        double sectorAFansPercent = 1.0*sectorACounter/fans*100;
        double sectorBFansPercent = 1.0*sectorBCounter/fans*100;
        double sectorVFansPercent = 1.0*sectorVCounter/fans*100;
        double sectorGFansPercent = 1.0*sectorGCounter/fans*100;
        System.out.printf("%.02f%%%n" +
                "%.02f%%%n" +
                "%.02f%%%n" +
                "%.02f%%%n" +
                "%.02f%%", sectorAFansPercent, sectorBFansPercent, sectorVFansPercent, sectorGFansPercent, allFansPercent);
    }
}
