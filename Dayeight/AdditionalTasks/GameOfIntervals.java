package Dayeight.AdditionalTasks;

import java.util.Scanner;

public class GameOfIntervals {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int round = Integer.parseInt(scan.nextLine());
        double totalResult =0.0;
        double fromNullToNine =0.0;
        double fromTenToNineteen =0.0;
        double fromTwentyToTwentynine = 0.0;
        double fromThirtyToThirtynine = 0.0;
        double fromFortyToFifty = 0.0;
        double invalidNumbers = 0.0;
        for (int i = 1; i <= round; i++){
            int number = Integer.parseInt(scan.nextLine());

            if (number>=0 && number<10){
                fromNullToNine++;
                totalResult += number*0.2;
            }else if (number>=10 && number<20){
                fromTenToNineteen++;
                totalResult += number*0.3;
            }else if (number>=20 && number<30){
                fromTwentyToTwentynine++;
                totalResult += number*0.4;
            }else if (number>=30 && number<40){
                fromThirtyToThirtynine++;
                totalResult += 50;
            }else if (number>=40 &&number<=50){
                fromFortyToFifty++;
                totalResult += 100;
            }else if (number<0 || number>50){
                invalidNumbers++;
                totalResult /= 2;
            }

        }
        double fromNullToNinePercent = fromNullToNine/round*100;
        double fromTenToNineteenPercent = fromTenToNineteen/round*100;
        double fromTwentyToTwentyninePercent = fromTwentyToTwentynine/round*100;
        double fromThirtyToThirtyninePercent = fromThirtyToThirtynine/round*100;
        double fromFortyToFiftyPercent = fromFortyToFifty/round*100;;
        double invalidNumbersPercent = invalidNumbers/round*100;
        System.out.printf("%.02f%n" +
                "From 0 to 9: %.02f%%%n" +
                "From 10 to 19: %.02f%%%n" +
                "From 20 to 29: %.02f%%%n" +
                "From 30 to 39: %.02f%%%n" +
                "From 40 to 50: %.02f%%%n" +
                "Invalid numbers: %.02f%%", totalResult, fromNullToNinePercent, fromTenToNineteenPercent, fromTwentyToTwentyninePercent,fromThirtyToThirtyninePercent,
                fromFortyToFiftyPercent, invalidNumbersPercent);
    }
}
