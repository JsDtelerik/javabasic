package Dayeight.AdditionalTasks;

import java.util.Scanner;

public class Grades {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int students = Integer.parseInt(scan.nextLine());
        int topStudentsCounter = 0;
        int goodStudentsCounter = 0;
        int acceptableStudentsCounter = 0;
        int lowStudentsCounter = 0;
        double gradeCounter = 0.0;
        for (int i = 1; i<=students; i++){
            double grades = Double.parseDouble(scan.nextLine());
            gradeCounter+=grades;
            if (grades>=5.00){
                topStudentsCounter++;
            }else if (grades>=4.00 && grades<5.00){
                goodStudentsCounter++;
            }else if (grades>=3.00 && grades<4.00){
                acceptableStudentsCounter++;
            }else{
                lowStudentsCounter++;
            }
        }
        double topStudentsPercent = 1.0*topStudentsCounter/students*100;
        double goodStudentsPercent = 1.0*goodStudentsCounter/students*100;
        double acceptableStudentsPercent = 1.0*acceptableStudentsCounter/students*100;
        double lowStudentsPercent = 1.0*lowStudentsCounter/students*100;

        System.out.printf("Top students: %.02f%%%n" +
                "Between 4.00 and 4.99: %.02f%%%n" +
                "Between 3.00 and 3.99: %.02f%%%n" +
                "Fail: %.02f%%%n" +
                "Average: %.02f",topStudentsPercent,goodStudentsPercent, acceptableStudentsPercent, lowStudentsPercent, (gradeCounter/students));



    }
}
