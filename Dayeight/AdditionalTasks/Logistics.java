package Dayeight.AdditionalTasks;

import java.util.Scanner;

public class Logistics {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = Integer.parseInt(scan.nextLine());
        double cargoPriceWithMinibus = 0.0;
        double cargoPriceWithTruck = 0.0;
        double cargoPriceWithTrain = 0.0;
        int cargoCounter = 0;
        for (int i = 1; i<=n; i++){
            int cargo = Integer.parseInt(scan.nextLine());
            cargoCounter+=cargo;
            if (cargo <= 3){
                cargoPriceWithMinibus += cargo*200.00;
            }else if (cargo>3 && cargo<=11){
                cargoPriceWithTruck += cargo*175;
            }else{
                cargoPriceWithTrain += cargo*120;
            }

        }

        System.out.printf("%.02f%n" +
                "%.02f%%%n" +
                "%.02f%%%n" +
                "%.02f%%%n", ((cargoPriceWithMinibus+cargoPriceWithTruck+cargoPriceWithTrain)/cargoCounter),
                (((cargoPriceWithMinibus/200)/cargoCounter)*100), (((cargoPriceWithTruck/175)/cargoCounter)*100), (((cargoPriceWithTrain/120)/cargoCounter)*100));
    }
}
