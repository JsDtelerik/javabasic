package Dayeight;

import java.util.Scanner;

public class HalfSumElement {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = Integer.parseInt(scan.nextLine());
        int maxNumber = Integer.MIN_VALUE;

        int numbersSum = 0;
        for (int i = 1; i <= n; i++) {
            int number = Integer.parseInt(scan.nextLine());
            numbersSum += number;
            if (maxNumber < number) {
                maxNumber = number;

            }
        }
            numbersSum -= maxNumber;
            if (maxNumber == numbersSum) {
                System.out.printf("Yes%n" +
                        "Sum = %d", maxNumber);
            } else {
                System.out.printf("No%n" +
                        "Diff = %d", Math.abs(maxNumber - numbersSum));
            }
        }
    }
