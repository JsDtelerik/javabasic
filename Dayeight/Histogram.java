package Dayeight;

import java.util.Scanner;

public class Histogram {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = Integer.parseInt(scan.nextLine());
        double p1 = 0.0;
        double p2 = 0.0;
        double p3 = 0.0;
        double p4 = 0.0;
        double p5 = 0.0;
        for (int i = 1; i<= n; i++){
            int number = Integer.parseInt(scan.nextLine());
                if (number<200){
                    p1++;
                }else if (number<400){
                    p2++;
                }else if (number<600){
                    p3++;
                }else if (number<800){
                    p4++;
                }else{
                    p5++;
                }
        }
            double p1Percent = p1/n*100;
            double p2Percent = p2/n*100;
            double p3Percent = p3/n*100;
            double p4Percent = p4/n*100;
            double p5Percent = p5/n*100;
        System.out.printf("%.2f%%%n" +
                          "%.2f%%%n" +
                          "%.2f%%%n" +
                          "%.2f%%%n" +
                          "%.2f%%%n", p1Percent, p2Percent, p3Percent, p4Percent, p5Percent);
    }
}
