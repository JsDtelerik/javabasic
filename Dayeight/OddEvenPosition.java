package Dayeight;

import java.util.Scanner;

public class OddEvenPosition {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double n = Double.parseDouble(scan.nextLine());
        double oddSum = 0.0;
        double oddMin = Integer.MAX_VALUE;
        double oddMax = Integer.MIN_VALUE;
        double evenSum = 0.0;
        double evenMin = Integer.MAX_VALUE;
        double evenMax = Integer.MIN_VALUE;

        for (int i = 1; i <= n; i++) {
            double number = Double.parseDouble(scan.nextLine());
            if (i % 2 == 0) {
                evenSum += number;
                if (number<evenMin) {
                    evenMin = number;
                }
                if(number>evenMax){
                    evenMax = number;
                }
            } else {
                oddSum += number;

                if (number<oddMin) {
                    oddMin = number;
                }
                if (number>oddMax){
                    oddMax = number;
                }
            }
        }

        if (oddSum == 0 && evenSum == 0){
            System.out.printf("OddSum=%.2f,%n" +
                    "OddMin=No,%n" +
                    "OddMax=No,%n" +
                    "EvenSum=%.2f,%n" +
                    "EvenMin=No,%n" +
                    "EvenMax=No%n",oddSum, evenSum);
        }else if (evenSum == 0){
            System.out.printf("OddSum=%.2f,%n" +
                    "OddMin=%.2f,%n" +
                    "OddMax=%.2f,%n" +
                    "EvenSum=%.2f,%n" +
                    "EvenMin=No,%n" +
                    "EvenMax=No%n",oddSum, oddMin, oddMax, evenSum);
        }else{
            System.out.printf("OddSum=%.2f,%n" +
                    "OddMin=%.2f,%n" +
                    "OddMax=%.2f,%n" +
                    "EvenSum=%.2f,%n" +
                    "EvenMin=%.2f,%n" +
                    "EvenMax=%.2f%n",oddSum, oddMin, oddMax, evenSum, evenMin, evenMax);
        }


    }
}