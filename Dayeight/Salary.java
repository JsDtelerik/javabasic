package Dayeight;

import java.util.Scanner;

public class Salary {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int tabs = Integer.parseInt(scan.nextLine());
        int salary = Integer.parseInt(scan.nextLine());
        int penalty = 0;
        String output = "";
        for (int i = 1; i<= tabs; i++){
            String tab = scan.nextLine();
            switch (tab){
                case "Facebook": penalty +=150; break;
                case "Instagram": penalty +=100; break;
                case "Reddit": penalty +=50; break;

                }
            if (salary<=penalty){
                System.out.println("You have lost your salary.");
                break;
            }
        }
        if (salary>penalty){
            System.out.printf("%d", salary-penalty);
        }

    }
}
