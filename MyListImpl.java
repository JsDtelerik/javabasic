package com.telerikacademy.oop;

import java.util.Arrays;
import java.util.Iterator;

public class MyListImpl<T> implements MyList<T> {

    private static int elementIndex;
    private T[] data;
    private int capacity;
    private int size;

    public MyListImpl() {

        this(4);
    }

    public MyListImpl(int capacity) {
        data = (T[]) new Object[capacity];
        this.setCapacity(capacity);
        size = 0;
    }

    public void setCapacity(int capacity) {
        if (capacity < 1) {
            throw new IllegalArgumentException("Capacity should be a positive number");
        }

        this.capacity = capacity;
    }


    @Override
    public int size() {

        return this.size;
    }

    @Override
    public int capacity() {
        return this.capacity;
    }

    @Override
    public void add(T element) {
        data[size++] = element;
        if (size >= capacity) {
            increaseCapacity();
        }
    }

    private void increaseCapacity() {
        this.capacity = capacity * 2;
        this.data = Arrays.copyOf(data, this.capacity);

    }

    @Override
    public T get(int index) {
        if (index >= 0 && index <= size - 1) {

            return data[index];
        }

        throw new IndexOutOfBoundsException(String.format("Index should be between 0 and %d", size));


    }


    @Override
    public int indexOf(T element) {

        iterFromIndex(element, 0, size - 1);
        return elementIndex;


    }

    @Override
    public int lastIndexOf(T element) {

        iterFromIndex(element, size - 1, 0);
        return elementIndex;

    }

    @Override
    public boolean contains(T element) {

        if (iterFromIndex(element, 0, size - 1)) {
            return true;
        }
        return false;
    }

    private boolean iterFromIndex(T element, int start, int stop) {
        elementIndex = -1;

        if (start == 0) {
            for (int i = start; i <= stop; i++) {

                if (data[i].equals(element)) {
                    elementIndex = i;
                    return true;
                }

            }

        } else {
            for (int i = start; i >= stop; i--) {

                if (data[i].equals(element)) {
                    elementIndex = i;
                    return true;
                }


            }

        }
        return false;
    }

    @Override
    public void removeAt(int index) {
        if (index >= 0 && index <= size - 1) {

            T[] copyOfData = (T[]) new Object[capacity];

            for (int i = 0, copyIndex = 0; i < size; i++) {
                if (index == i) {
                    continue;
                }
                copyOfData[copyIndex++] = data[i];
            }

            size--;

            if (capacity / 2 >= size) {

                decreaseCapacity();
            }


            data = Arrays.copyOf(copyOfData, capacity);

        } else {
            throw new ArrayIndexOutOfBoundsException("Index out of bound.");
        }


    }


    @Override
    public boolean remove(T element) {

        if (contains(element)) {

            removeAt(elementIndex);
            return true;
        }


        return false;
    }

    private void decreaseCapacity() {

        this.capacity = capacity / 2;
    }


    @Override
    public void clear() {

        this.data = (T[]) new Object[capacity];

        size = 0;

    }

    @Override
    public void swap(int from, int to) {

        T element = data[from];

        data[from] = data[to];
        data[to] = element;
    }

    @Override
    public void print() {

        T[] limitedArr = Arrays.copyOf(data, size);

        System.out.println(Arrays.toString(limitedArr));
    }

    @Override
    public Iterator<T> iterator() {


        return new MyListIterator();
    }



private class MyListIterator implements Iterator<T> {

    private int currentIndex;

    MyListIterator() {
        currentIndex = 0;
    }

    @Override
    public boolean hasNext() {
        return currentIndex < size;
    }

    @Override
    public T next() {
        T currentElement = data[currentIndex];
        currentIndex++;
        return currentElement;
    }
}
}
